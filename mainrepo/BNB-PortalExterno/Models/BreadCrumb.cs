﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class BreadCrumb
  {
    public bool TieneImagen { get; set; }
    public string RutaImagen { get; set; }
    public string Accion { get; set; }
    public string Controlador { get; set; }
    public string Area { get; set; }
    public string Desripcion { get; set; }
    public int Id { get; set; }
  }
}