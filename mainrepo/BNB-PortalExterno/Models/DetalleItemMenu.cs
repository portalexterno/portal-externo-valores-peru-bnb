﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class DetalleItemMenu
  {
    public int Id { get; set; }
    public int IdItemMenu { get; set; }
    public string Encabezado { get; set; }
    public string Descripcion { get; set; }
    public List<string> DetalleDescripcion { get; set; }
    public string UrlImagen { get; set; }
    public List<TipoLista> Caracteristicas { get; set; }
    public List<string> Restricciones { get; set; }
    public List<string> Requisitos { get; set; }
    public List<string> Beneficios { get; set; }
    public List<TipoLista> RecomendacionesSeguridad { get; set; }
    public List<string> CondicionesEspeciales { get; set; }
    public Enumeradores.TipoDetalle TipoDetalle { get; set; }
    public List<Descarga> Descargables { get; set; }
  }
}