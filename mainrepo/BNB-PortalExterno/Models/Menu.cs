﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Menu
  {
    public int Id { get; set; }
    public int IdSuperior { get; set; }
    public string Nombre { get; set; }
    public string TituloBanner { get; set; }
    public string Descripcion { get; set; }
    public string Banner { get; set; }
    public bool TieneImagen { get; set; }
    public string RutaImagen { get; set; }
    public bool TieneIcono { get; set; }
    public string RutaIcono { get; set; }
    public string RutaIconoImagen { get; set; }
    public string Accion { get; set; }
    public string Controlador { get; set; }
    public string Area { get; set; }
    public int Orden { get; set; }
    public Enumeradores.TipoMenu TipoMenu { get; set; }
    public bool EstaSeleccionado { get; set; }
    /// <summary>
    /// Breadcrumb o ruta del item desde el menu inicio.
    /// </summary>
    public List<Models.BreadCrumb> BreadCrumbs { get; set; }
    /// <summary>
    /// Items dependientes
    /// </summary>
    public List<Models.Menu> ItemMenu { get; set; }
    /// <summary>
    /// Detalle(s) de los items
    /// </summary>
    public List<Models.DetalleItemMenu> DetalleMenuItem { get; set; }
  }
}