﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Persona
  {
    [Display(Name = "Nombres")]
    [Required(ErrorMessage = "Información requerida.")]
    public string Nombres { get; set; }

    [Display(Name = "Apellido Paterno")]
    [Required(ErrorMessage = "Información requerida.")]
    public string ApellidoPaterno { get; set; }

    [Display(Name = "Apellido Materno")]
    [Required(ErrorMessage = "Información requerida.")]
    public string ApellidoMaterno { get; set; }

    [Display(Name = "Apellido Casada(opc)")]
    public string ApellidoCasada { get; set; }

    [Display(Name = "Número de Documento")]
    [Required(ErrorMessage = "Información requerida")]
    [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Por favor solo ingrese números")]
    public string NumeroDocumento { get; set; }

    [Display(Name = "Complemento")]
    [StringLength(2, ErrorMessage = "El complemento solo debe contener dos caracteres")]
    public string Complemento { get; set; }

    [Display(Name = "Extensión")]
    [Required(ErrorMessage = "por favor seleccione.")]
    public int CodigoExtencionDocumento { get; set; }

    [Display(Name = "Género")]
    [Required(ErrorMessage = "por favor seleccione")]
    public int CodigoGenero { get; set; }

    [Display(Name = "Estado civil")]
    [Required(ErrorMessage = "por favor seleccione")]
    public int CodigoEstadoCivil { get; set; }

    [Display(Name = "Fecha de nacimiento")]
    [Required(ErrorMessage = "por favor seleccione")]
    public DateTime FechaNacimiento { get; set; }

    [Display(Name = "Ingresa tu edad")]
    [Required(ErrorMessage = "por favor seleccione")]
    [Range(18, 80, ErrorMessage = "tu edad debe esta entre los 18 y 80 años")]
    public int Edad { get; set; }

    [Display(Name = "Es ciudadano Estado Unidense")]
    public bool EsCiudadanoEEUU { get; set; }

    public string DocumentoResidenciaEEUU { get; set; }

    [Display(Name = "Desea declarar algún tipo de discapacidad")]
    [Required(ErrorMessage = "por favor seleccione.")]
    public int TipoIncapacidad { get; set; }

    [Display(Name = "Descripción")]
    public string Incapacidad { get; set; }

    [Display(Name = "Teléfono fijo (opcional)")]
    public string Telefono { get; set; }
    [Display(Name = "Teléfono celular")]
    [Required(ErrorMessage = "Información requerida")]
    [StringLength(8, MinimumLength = 8, ErrorMessage = "Por favor ingrese un numero valido.")]
    public string Celular { get; set; }
    [Display(Name = "Correo electrónico - Email")]
    [Required(ErrorMessage = "Información requerida")]
    [EmailAddress(ErrorMessage = "Ingrese un correo electrónico valido.")]
    public string CorreoElectronico { get; set; }

    public bool EsCliente { get; set; }

    [Display(Name = "¿Tiene referido?")]
    public bool TieneReferido { get; set; }
    public string NombreReferido { get; set; }

    [Display(Name = "Nombre completo")]
    [Required(ErrorMessage = "Información requerida.")]
    public virtual String NombreCompleto
    {
      get
      {
        string nomb = (this.ApellidoPaterno + " " + this.ApellidoMaterno + " " + this.Nombres).Replace("  ", " ");
        nomb = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(nomb.ToLower());
        return nomb;
      }
      set {

      }
    }
  }
}