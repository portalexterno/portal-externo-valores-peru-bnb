﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Constantes
  {
    public const string MENU = "Menu";
    public const string SUBMENU = "SubMenu";
    public const string DETALLES_ITEM_MENU = "DetalleItemMenu";
    public const string FORMULARIO_REGISTRO_TARJETA_CREDITO = "FormularioRegistroTarjetaCredito";
    public const string FORMULARIO_REGISTRO_VIVIENDA_SOCIAL = "FormularioRegistroViviendaSocial";
    public const string PARAMETROS = "Parametros";


    public const int MENU_BANCA_PERSONAS = 1;
    public const int MENU_BANCA_EMPRESAS = 2;
    public const int MENU_BANCA_GRUPO = 3;

    public const int MENU_SECCION_BANCA_PERSONAS = 1;
    public const int MENU_SECCION_CREDITOS = 5;
    public const int MENU_SECCION_TARJETAS_CREDITO = 6;
    public const int MENU_SECCION_AHORRO_INVERSION = 7;
    public const int MENU_SECCION_SERVICIOS = 8;
    public const int MENU_SECCION_SEGUROS = 9;
  public const int MENU_SECCION_NOTICIAS = 14;

  public const int MENU_VIVIENDA = 10;
    public const int MENU_VEHICULOS = 11;
    public const int MENU_CONSUMO = 12;
    public const int MENU_LINEA_CREDITO = 13;


    public const int ITEM_TARJETA_CREDITO_EXCELLENCE = 18;
    public const int ITEM_TARJETA_CREDITO_SIGNATURE = 19;
    public const int ITEM_TARJETA_CREDITO_INTERNATIONAL = 20;
    public const int ITEM_TARJETA_CREDITO_PLATINUM = 21;
    public const int ITEM_TARJETA_CREDITO_BANCA_JOVEN_PLUS = 22;
    public const int ITEM_TARJETA_CREDITO_CASH = 23;

    public const int ITEN_SEGUROS_PROTECCION_FINANCIERA = 90;
    public const int ITEN_SEGUROS_ENFERMEDADES_GRAVES = 91;
    public const int ITEN_SEGUROS_CESANTIA = 92;
    public const int ITEN_SEGUROS_ACCIDENTES_PERSONALES = 93;

    public const int ITEM_NEGOCIACION_VALORES = 101;
    public const int ITEM_CUSTODIO_DESMATERIALIZACION = 102;
    public const int ITEM_ASESORIA_INVERSIONES = 103;
		public const int ITEM_ASESORIA_PATRIMONIAL = 104;


		public const int ITEM_ESTRUCTURACIONES = 121;
		public const int ITEM_COLOCACION_VALORES = 122;
		public const int ITEM_CUSTODIA_VALORES = 123;
		public const int ITEM_SERVICIO_REPRESENTANTE = 124;
		public const int ITEM_VALORIZACIONES_ASESORIA = 125;
		public const int ITEM_NEGOCIACION_CORP = 126;
		





		public const int ITEM_CREDITO_VEHICULO = 111;
    public const int ITEM_CREDITO_VEHICULO_BANCA_JOVEN = 112;

    public const int ITEM_CREDITO_CONSUMO_BANCA_JOVEN = 121;
    public const int ITEM_CREDITO_CONSUMO_7X5 = 122;
    public const int ITEM_CREDITO_CONSUMO_TU_ELECCION = 123;
    public const int ITEM_CREDITO_CONSUMO_BANCA_SENIOR = 124;

    public const int ITEM_CAJA_DE_AHORRO_DIGITAL = 141;
    public const int ITEM_CAJA_DE_AHORRO_BJ = 142;
    public const int ITEM_CAJA_DE_AHORRO_BJP = 143;
    public const int ITEM_CAJA_DE_AHORRO_EF = 144;
    public const int ITEM_CAJA_DE_AHORRO_CLASICA = 145;
    public const int ITEM_CAJA_DE_AHORRO_BS = 146;

    public const int ITEM_CC_MN = 151;
    public const int ITEM_CC_ME = 152;
    public const int ITEM_CC_ADELANTO = 153;

    public const int ITEM_DPF_ME = 171;
    public const int ITEM_DPF_MN = 172;


    public const int ITEM_CREDITO_FAMILIAR = 131;
    public const int ITEM_CREDITO_BANCA_JOVEN = 132;

    public const int MENU_SERVICIOS_DIGITALES = 83;
    public const int MENU_TARJETA_EXCELLENCE = 18;

    //Servicios
    public const int ITEM_SERVICIOS_GIROS_INTERNACIONALES = 821;
    public const int ITEM_SERVICIOS_CARTA_CREDITO_EXPORTACION = 822;
    public const int ITEM_SERVICIOS_BOLETA_GARANTIA = 823;
    public const int ITEM_SERVICIOS_DOCUMENTO_COBRANZA = 824;
    public const int ITEM_SERVICIOS_CHEQUE_EXTERIOR = 825;
    public const int ITEM_SERVICIOS_BOLETA_GARANTIA_CONTRATO = 826;
    public const int ITEM_SERVICIOS_CUSTODIA_VALORES_INTERNACIONAL = 827;

    public const int ITEM_SERVICIOS_GIROS_LOCALES = 811;
    public const int ITEM_SERVICIOS_FIDEICOMISOS = 812;
    public const int ITEM_SERVICIOS_PUNTO_RECLAMO = 813;
    public const int ITEM_SERVICIOS_CERTIFICACION_CHEQUES = 814;
    public const int ITEM_SERVICIOS_CHEQUES_GERENCIA = 815;
    public const int ITEM_SERVICIOS_COBRANZA_SERVICIOS = 816;
    public const int ITEM_SERVICIOS_CUSTODIA_VALORES = 817;

    public const int ITEM_SERVICIOS_BNB_MOVIL = 831;
    public const int ITEM_SERVICIOS_BNB_NET = 832;
    public const int ITEM_SERVICIOS_BNB_CAJEROS = 833;
    public const int ITEM_SERVICIOS_BNB_CARLITOS_BNB = 834;
    public const int ITEM_SERVICIOS_BNB_BILLETERA_ELECTRONICA = 835;
    public const int ITEM_SERVICIOS_BNB_CONTACT_CENTER = 837;
    public const int ITEM_SERVICIOS_BNB_COBRANZA = 838;
    public const int ITEM_SERVICIOS_BNB_EN_TU_BARRIO = 839;
    public const int ITEM_SERVICIOS_BNB_BIENES_ADJUDICADOS = 840;
    public const int ITEM_SERVICIOS_BNB_FACEBANKING = 841;


    //Banca Empresas
    public const int MENU_SECCION_CORPORATIVO = 201;
    public const int MENU_SECCION_MYPE = 202;
    public const int MENU_SECCION_PYME = 203;

    public const int ITEM_MYPE_NEGOCIOS = 205;
    public const int ITEM_MYPE_FAMILIA = 206;
    public const int ITEM_MYPE_VIVIENDA = 207;
    public const int ITEM_MYPE_VEHICULO = 208;

    public const int ITEM_PYME_CRECER = 209;
    public const int ITEM_PYME_IMPULSAR = 210;

    public const int ITEM_CORPORATIVO_AVAL = 211;
    public const int ITEM_CORPORATIVO_FONDOGARANTIA = 212;
    public const int ITEM_CORPORATIVO_CUENTACORRIENTE = 213;
    public const int ITEM_CORPORATIVO_DPF = 214;
    public const int ITEM_CORPORATIVO_ACH = 215;
    public const int ITEM_CORPORATIVO_CAJA_SEGURIDAD = 216;
    public const int ITEM_CORPORATIVO_ADELANTO_CUENTA = 217;
    public const int ITEM_CORPORATIVO_PAGO_SERVICIOS = 218;

    //Grupo BNB
    public const int ITEM_GRUPO_BNB = 219;
    public const int ITEM_GRUPO_SAFI = 220;
    public const int ITEM_GRUPO_LEASING = 221;
    public const int ITEM_GRUPO_VALORES = 222;

    public const int ITEM_SAFI_EFECTIVO = 223;
    public const int ITEM_SAFI_PORTAFOLIO = 224;
    public const int ITEM_SAFI_OPORTUNO = 225;
    public const int ITEM_SAFI_OPCION = 226;
    public const int ITEM_SAFI_ACCION = 227;
    public const int ITEM_SAFI_INTERNACIONAL = 228;
    public const int ITEM_SAFI_FUTURO = 229;
    public const int ITEM_SAFI_GLOBAL = 230;

    public const int ITEM_VALORES_ASESORIAFINANCIERA = 231;
    public const int ITEM_VALORES_BANCAPRIVADA = 232;
    public const int ITEM_VALORES_EMISIONESVALORES = 233;
    public const int ITEM_VALORES_OPERACIONESBURSATILES = 234;

    public const int ITEM_TARJETA_CREDITO_PUNTOS = 235;
    public const int ITEM_TARJETA_CREDITO_ADVANTAGE = 236;
    public const int ITEM_TARJETA_CREDITO_REWARDS = 237;

    public const int MENU_RESPONSABILIDAD_SOCIAL = 238;
    public const int MENU_RSE_NORMATIVA = 239;
    public const int MENU_RSE_PROGRAMAS = 240;
    public const int MENU_RSE_PUBLICACIONES = 241;
    public const int MENU_RSE_PACTO_GLOBAL = 242;

    //SAFI
    public const int MENU_SAFI_BOL_OPORTUNO = 411;
    public const int MENU_SAFI_BOL_OPCION = 412;
    public const int MENU_SAFI_BOL_ACCION = 413;
    public const int MENU_SAFI_USD_ASEGURADO = 414;
    public const int MENU_SAFI_USD_PORTAFOLIO = 421;
    public const int MENU_SAFI_USD_EFECTIVO = 422;

    public const int MENU_LEASING_PER_VIVE = 511;
    public const int MENU_LEASING_PER_AUTO = 512;
    public const int MENU_LEASING_EMP_MAQUINARIA = 521;
    public const int MENU_LEASING_EMP_VEHICULO = 522;
    public const int MENU_LEASING_EMP_MEDICO = 523;
    public const int MENU_LEASING_EMP_INMUEBLES = 524;

    public const int MENU_VALORES_PER_REPORTO = 611;
    public const int MENU_VALORES_PER_2 = 612;

    public const int MENU_VALORES_PER_RENTAFIJA = 6121;
    public const int MENU_VALORES_PER_ACCIONES = 6122;

    public const int MENU_VALORES_EMP_EMISIONES = 621;
    public const int MENU_VALORES_EMP_INVERSIONES = 622;
    public const int MENU_VALORES_EMP_ADMINISTRACIONCARTERA = 623;
    public const int MENU_VALORES_EMP_4 = 624;





  }
}