﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Direccion
  {
    [Display(Name = "Dirección de Contacto")]
    [Required(ErrorMessage = "Información requerida.")]
    public string Domicilio { get; set; }
  }
}