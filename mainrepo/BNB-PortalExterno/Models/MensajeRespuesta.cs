﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class MensajeRespuesta
  {
    public string Descripcion { get; set; }
    public string Mensaje { get; set; }
    public bool OcurrioError { get; set; }
    public Enumeradores.TipoError TipoError { get; set; }
  }
}