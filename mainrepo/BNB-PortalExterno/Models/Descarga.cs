﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Descarga
  {
    public int Id { get; set; }
    public int Orden { get; set; }
    public string Nombre { get; set; }
    public string Ruta { get; set; }
  }
}