﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
    public class Enumeradores
    {
        public enum Parametros
        {
            PRM_Paises = 1,
            PRM_Departamentos = 2,
            PRM_Ciudades = 3,
            PRM_Provincias = 4,
            PRM_Genero = 5,
            PRM_Extencion_Documento = 6,
            PRM_Estado_Civil = 7,
            PRM_Nivel_Educacion = 8,
            PRM_Tipo_Profesion = 9,
            PRM_Ingreso_Anual = 10,
            PRM_Descripcion_Direccion = 11,
            PRM_Descripcion_Zona = 12,
            PRM_Situacion_Laboral = 13,
            PRM_Actividad_Economica = 14,
            PRM_Tipo_Referencia = 15,
            PRM_Tipo_Empresa = 16,
            PRM_Rubro = 17,
            PRM_Sub_Rubro = 18,
            PRM_Tipo_Discapacidad = 19,
            PRM_Tipo_Cargo_PEP = 20,
            PRM_Instituciones_PEP = 21,
            PRM_Modelos_Tarj_Debito = 22,
            PRM_Tipo_Tarjeta_Debito = 23,
            PRM_Productos = 24,
            PRM_Sub_Producto = 25,
            PRM_Moneda = 26,
            PRM_Tipo_Cuenta = 27,
            PRM_Cantidad_Cheques = 28,
            PRM_Tipo_Documento_Persona_Natural = 29,
            PRM_Agencias = 30,
            PRM_Oficinas = 31,
            PRM_TipoBien = 32,
            PRM_Actividad_Economica_CIIU = 33,
            PRM_SINO = 35,
            PRM_Tipo_Comentario = 36,
            PRM_Tipo_Tema = 37,
            PRM_Tipo_Tasa_Base = 38,
            PRM_Tipo_Cre_Viv = 39,
            PRM_Tipo_Prop = 40
                
        }
        public enum TipoDocumento : int
        {
            NoDefinido = -1,
            Otro = 0,
            CI = 1,
            NIT = 12
        }
        public enum TipoMenu
        {
            Principal = 1,
            Menu = 2,
            SubMenu = 3,
            item = 4,
            SubItem = 5,
            Complementario = 6
        }

        public enum TipoTarjetaCredito
        {
            SinTarjeta = 0,
            Clasica = 1,
            BancajovenPlus = 2,
            BNBCash = 3,
            Platino = 4,
            Excellence = 5
        }

        public enum TipoError
        {
            Error = 1,
            Advertencia = 2
        }

        public enum TipoErrorMovil : int
        {
            Token = 1,
            Otro = 2
        }

        public enum TipoDetalle
        {
            Generico = 0,
            Compra = 1,
            Construccion = 2,
            Anticretico = 3,
            Nuevo = 4,
            Usado = 5
        }

        public enum Moneda : int
        {
            Bs = 0,
            USD = 1

        }
        public enum TipoCanal : int
        {
            Desconocido = 0,          
            PortalExterno = 7
        }
    }
}