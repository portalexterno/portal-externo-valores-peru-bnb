﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Noticia
  {
    public int Id { get; set; }
    public string Titular { get; set; }
    public string SubTitular { get; set; }
    public string Descripcion { get; set; }
    public List<TipoLista> Contenido { get; set; }
    public List<string> UrlImagenes { get; set; }
    public List<string> UrlVideos { get; set; }
    public string EnlaceExterno { get; set; }
    public DateTime FechaPublicacion { get; set; }
  }
}