﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Comun
  {
    [Serializable()]
    public class RespuestaGenericaSinMensaje
    {
      public Enumeradores.TipoErrorMovil TipoError { get; set; }
    }

    [Serializable()]
    public abstract class EnvioGenerico
    {
      public string Token { get; set; }
      public string Origen { get; set; }
    }
  }
}