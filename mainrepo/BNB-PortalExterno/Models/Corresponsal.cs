﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Corresponsal
  {
    public int codSucursal { get; set; }
    public string Nombre { get; set; }
    public string Direccion { get; set; }
    public string Horarios { get; set; }
    public string fullName { get; set; }    
  }
}