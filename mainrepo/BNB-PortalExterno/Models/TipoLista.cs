﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class TipoLista
  {
    public string Encabezado { get; set; }
    public string Descripcion { get; set; }
    public List<string> Contenidos { get; set; }
  }
}