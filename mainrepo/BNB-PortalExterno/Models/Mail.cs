﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
    public class Mail
    {
        public class RequestMail
        {
            public string emailAccount { get; set; }
            public string displayName { get; set; }
            public string emailUser { get; set; }
            public string emailPassword { get; set; }
            public List<string> recipients { get; set; }
            public string CCRecipients { get; set; }
            public string CCORecipients { get; set; }
            public string subject { get; set; }
            public string messageTitle { get; set; }
            public string message { get; set; }
            public Dictionary<string, string> importantData { get; set; }
            public Dictionary<string, string> attachedFiles { get; set; }
            public string logo { get; set; }
        }

        public class ResponseMail
        {
            public bool success { get; set; }
            public string message { get; set; }
            public long code { get; set; }
            public Exception technicalDetail { get; set; }
        }
    }
}