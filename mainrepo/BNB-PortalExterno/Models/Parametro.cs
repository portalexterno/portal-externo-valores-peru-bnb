﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Models
{
  public class Parametro
  {
    public int Codigo { get; set; }
    public int CodigoSuperior { get; set; }
    public string Descripcion { get; set; }
    public string Abreviacion { get; set; }
    public Enumeradores.Parametros GrupoParametro { get; set; }
  }
}