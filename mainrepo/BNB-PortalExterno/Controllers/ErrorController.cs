﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BNB_PortalExterno.Controllers
{
    public class ErrorController : Controller
    {
    public ActionResult Error(string error)
    {
      return View();
    }
    public ActionResult ErrorModal(string error)
    {
      return PartialView();
    }
    public ActionResult PageNotFound()
    {
      Response.StatusCode = (int)HttpStatusCode.NotFound;
      return View();
    }
    public ActionResult CustomError()
    {
      Response.StatusCode = (int)HttpStatusCode.InternalServerError;
      return View();
    }
    public ActionResult Unauthorized()
    {
      Response.StatusCode = (int)HttpStatusCode.Unauthorized;
      return View();
    }
    public ActionResult Forbidden()
    {
      Response.StatusCode = (int)HttpStatusCode.Forbidden;
      return View();
    }
  }
}