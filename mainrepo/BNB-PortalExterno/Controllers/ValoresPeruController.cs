﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BNB_PortalExterno.Controllers
{
	public class ValoresPeruController : Controller
	{
		// GET: ValorePeru
		public ActionResult Nosotros(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);

				//Limpia la lista de los item seleccionados
				var Menu = (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu != Models.Enumeradores.TipoMenu.Principal);
				Menu.FindAll(x => x.EstaSeleccionado = false);
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
					MenuItem.EstaSeleccionado = true;
				}
				//Verificar si el menu tiene sub menu
				if ((HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Exists(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu))
				{
					List<Models.Menu> SubMenu = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu).ToList();
					HttpContext.Session[Models.Constantes.SUBMENU] = SubMenu;
				}
				else
				{
					HttpContext.Session[Models.Constantes.SUBMENU] = null;
				}
			}
			return View(MenuItem);
		}

		public ActionResult NuestrosServicios(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);

				//Limpia la lista de los item seleccionados
				var Menu = (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu != Models.Enumeradores.TipoMenu.Principal);
				Menu.FindAll(x => x.EstaSeleccionado = false);
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
					MenuItem.EstaSeleccionado = true;
				}
				//Verificar si el menu tiene sub menu
				if ((HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Exists(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu))
				{
					List<Models.Menu> SubMenu = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu).ToList();
					HttpContext.Session[Models.Constantes.SUBMENU] = SubMenu;
				}
				else
				{
					HttpContext.Session[Models.Constantes.SUBMENU] = null;
				}
			}
			return View(MenuItem);
		}

		#region Productos
		public ActionResult Naturales(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					// var Menu = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu == TipoRespuesta.Item.TipoMenu);
					HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu == TipoRespuesta.Item.TipoMenu);
					var Menu = (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu == TipoRespuesta.Item.TipoMenu);
					Menu.FindAll(x => x.EstaSeleccionado = false);
					// Obtener la informacion la lista del mismo tipo 
					TipoRespuesta.Item.EstaSeleccionado = true;
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult Corporativos(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					// var Menu = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu == TipoRespuesta.Item.TipoMenu);
					HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu == TipoRespuesta.Item.TipoMenu);
					var Menu = (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu == TipoRespuesta.Item.TipoMenu);
					Menu.FindAll(x => x.EstaSeleccionado = false);
					// Obtener la informacion la lista del mismo tipo 
					TipoRespuesta.Item.EstaSeleccionado = true;
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult NegociacionValores(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult CustorioDesmaterializacion(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult AsesoriaInversiones(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult AsesoriaPatrimonial(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}


		public ActionResult Estructuraciones(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult Colocaciones(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult CustodiaValores(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult ServicioRepresentante(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult ValorizacionesAsesoria(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}

		public ActionResult NegociacionValoresCorp(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
				//Obtener el detalle del item
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
				}
			}
			return View(MenuItem);
		}
		#endregion


		public ActionResult RegistroClientes(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);

				//Limpia la lista de los item seleccionados
				var Menu = (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu != Models.Enumeradores.TipoMenu.Principal);
				Menu.FindAll(x => x.EstaSeleccionado = false);
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
					MenuItem.EstaSeleccionado = true;
				}
				//Verificar si el menu tiene sub menu
				if ((HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Exists(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu))
				{
					List<Models.Menu> SubMenu = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu).ToList();
					HttpContext.Session[Models.Constantes.SUBMENU] = SubMenu;
				}
				else
				{
					HttpContext.Session[Models.Constantes.SUBMENU] = null;
				}
			}
			return View(MenuItem);
		}

		public ActionResult Contactos(int Id)
		{
			Models.Menu MenuItem = new Models.Menu();
			//Verificar si la opcion tiene sub menu y obtener la informacion para generar el breadcrumb
			if (Id != 0)
			{
				MenuItem = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);

				//Limpia la lista de los item seleccionados
				var Menu = (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu != Models.Enumeradores.TipoMenu.Principal);
				Menu.FindAll(x => x.EstaSeleccionado = false);
				Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio = new Componentes.Metodos.Menu.ObtenerPorId.TipoEnvio();
				TipoEnvio.Id = Id;
				Componentes.Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = Componentes.LogicaNegocios.Menu.ObtenerPorId(TipoEnvio);
				if (!TipoRespuesta.OcurrioError)
				{
					MenuItem = TipoRespuesta.Item;
					MenuItem.EstaSeleccionado = true;
				}
				//Verificar si el menu tiene sub menu
				if ((HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Exists(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu))
				{
					List<Models.Menu> SubMenu = (HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.IdSuperior == Id && x.TipoMenu == Models.Enumeradores.TipoMenu.SubMenu).ToList();
					HttpContext.Session[Models.Constantes.SUBMENU] = SubMenu;
				}
				else
				{
					HttpContext.Session[Models.Constantes.SUBMENU] = null;
				}
			}
			return View(MenuItem);
		}

		public ActionResult Noticias(int Id)
		{

			return View();
		}

	}
}