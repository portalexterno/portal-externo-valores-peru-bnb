﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BNB_PortalExterno.Controllers
{
  public class PrincipalController : Controller
  {
    public ActionResult Index(int Id = 1)
    {
      Models.Menu MenuItem = new Models.Menu();
      if ((HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Exists(x => x.Id == Id))
      {
        HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] = HttpContext.Application[BNB_PortalExterno.Models.Constantes.MENU];
        //Limpia la lista de los item seleccionados
        (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.EstaSeleccionado = false);
        //Obtiene la informacion del menu y lo envia a la vista
        MenuItem = (HttpContext.Session[BNB_PortalExterno.Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == Id);
        MenuItem.EstaSeleccionado = true;
      }
      HttpContext.Session[Models.Constantes.SUBMENU] = null;
      return View(MenuItem);
    }


		public ActionResult FormReclamo()
		{
			return View();
		}

		#region VistarParciales
		public ActionResult _Header(string logo)
		{
			//Obtener la informacion del menu principal
			ViewBag.logoHeader = logo;
			List<Models.Menu> Menu = HttpContext.Session[Models.Constantes.MENU] as List<Models.Menu>;
			if (Menu.Exists(x => x.TipoMenu == Models.Enumeradores.TipoMenu.Principal))
			{
				var menuHeader = Menu.FindAll(x => x.TipoMenu == Models.Enumeradores.TipoMenu.Principal).ToList();
				return PartialView(menuHeader);
			}
			return PartialView();
		}

		public ActionResult _HeaderSection(int Id)
		{
			//Obtener la información del menu por el id
			if ((HttpContext.Application[Models.Constantes.MENU] as List<Models.Menu>).Exists(x => x.Id == Id))
			{
				HttpContext.Session[Models.Constantes.MENU] = HttpContext.Application[Models.Constantes.MENU];
				var menus = (HttpContext.Session[Models.Constantes.MENU] as List<Models.Menu>).FindAll(x => x.TipoMenu == Models.Enumeradores.TipoMenu.Principal || x.TipoMenu == Models.Enumeradores.TipoMenu.Complementario);
				menus.FindAll(x => x.EstaSeleccionado = false);
				var menu = (menus).Find(x => x.Id == Id);
				if (menu != null)
				{
					menu.EstaSeleccionado = true;
				}
				else
				{
					menus.First().EstaSeleccionado = true;
				}
				return PartialView(menu);
			}
			return PartialView();
		}
		public ActionResult _Footer()
		{
			return PartialView();
		}
		public ActionResult _AccesoBnb()
		{
			return PartialView();
		}

		public ActionResult _AvisoInicio()
		{
			return PartialView();
		}

		#endregion



		[HttpPost]
		public JsonResult RegistrarReclamo(string a0, string a1, string a2, string a3, string a4, String a5, DateTime a6, string a7)
		{
			Models.Reclamo formReclamo = new Models.Reclamo();
			formReclamo.Nombres = a0;
			formReclamo.TipoDocumento = a1;
			formReclamo.NumeroDocumento = a2;
			formReclamo.Email = a3;
			formReclamo.Direccion = a4;
			formReclamo.Telefono = a5;
			formReclamo.FechaIncidencia = a6;
			formReclamo.DetalleReclamo = a7;

			Models.MensajeRespuesta response = new Models.MensajeRespuesta();
			Componentes.LogicaNegocios.MailServices mailServices = new Componentes.LogicaNegocios.MailServices();
			response = mailServices.SendMailReclamo(formReclamo);


			return Json(new
			{

				Correcto = !response.OcurrioError,
				Mensaje = response.Mensaje
			});
		}

	}
}