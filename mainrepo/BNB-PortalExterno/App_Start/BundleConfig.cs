﻿using System.Web;
using System.Web.Optimization;

namespace BNB_PortalExterno
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Jquery
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));

            //Bootstrap
            bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include("~/Content/bootstrap.css"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrapjs").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js"));

            //SweetAlert
            bundles.Add(new StyleBundle("~/Content/sweetalertcss").Include("~/Content/Sweetalert/sweetalert2.css", "~/Content/sweetalert/bnb.css"));
            bundles.Add(new ScriptBundle("~/bundles/sweetalertjs").Include("~/Scripts/Sweetalert/sweetalert2.js"));

            //Cards
            bundles.Add(new StyleBundle("~/Content/rccss").Include("~/Content/rotating-card.css"));
            bundles.Add(new StyleBundle("~/Content/fccss").Include("~/Content/floating-card.css"));
            bundles.Add(new StyleBundle("~/Content/sccss").Include("~/Content/showing-card.css"));

            //Ajax calls
            bundles.Add(new ScriptBundle("~/bundles/ajaxjs").Include("~/Scripts/jquery.validate.js", "~/Scripts/jquery.validate.unobtrusive.js", "~/Scripts/jquery.form.js", "~/Scripts/jquery.unobtrusive-ajax.js"));


            //Estilos y scripts del portal
            bundles.Add(new StyleBundle("~/Content/portalcss").Include("~/Content/site.css", "~/Content/portal-externo/portal-externo.css"));
            bundles.Add(new ScriptBundle("~/bundles/portaljs").Include("~/Scripts/Portal-externo/portal-externo.js"));

            bundles.Add(new StyleBundle("~/Content/footer-sectioncss").Include("~/Content/portal-externo/footer-section.css"));
            bundles.Add(new StyleBundle("~/Content/header-sectioncss").Include("~/Content/portal-externo/header-section.css"));
            bundles.Add(new ScriptBundle("~/bundles/header-sectionjs").Include("~/Scripts/Portal-externo/header-section.js"));

            //Animaciones
            bundles.Add(new StyleBundle("~/Content/animatedcss").Include("~/Content/animations/animate.css"));
            bundles.Add(new StyleBundle("~/Content/aoscss").Include("~/Content/animations/aos.css"));
            bundles.Add(new ScriptBundle("~/bundles/aosjs").Include("~/Scripts/animations/aos.js"));

            //tabs
            bundles.Add(new StyleBundle("~/Content/tabscss").Include("~/Content/tabs/bootstrap-responsive-tabs.css"));
            bundles.Add(new StyleBundle("~/Content/tabs2css").Include("~/Content/tabs/bootstrap-responsive-tabs-2.css"));
            bundles.Add(new StyleBundle("~/Content/tabs3css").Include("~/Content/tabs/bootstrap-responsive-tabs-3.css"));
            bundles.Add(new ScriptBundle("~/bundles/tabsjs").Include("~/Scripts/tabs/jquery.bootstrap-responsive-tabs.min.js"));
            bundles.Add(new StyleBundle("~/Content/tabioncss").Include("~/Content/tabs/tabion.css", "~/Content/tabs/animate-custom.css"));

            //Owl carrousel
            bundles.Add(new StyleBundle("~/Content/occss").Include("~/Content/owlcarrousel/owl.carousel.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/ocjs").Include("~/Scripts/owlcarrousel/owl.carousel.min.js"));

            //Swiper
            bundles.Add(new StyleBundle("~/Content/swcss").Include("~/Content/swiper/swiper.css"));
            bundles.Add(new ScriptBundle("~/bundles/swjs").Include("~/Scripts/swiper/swiper.js"));

            //time-Line
            bundles.Add(new StyleBundle("~/Content/timelinecss").Include("~/Content/time-line/time-line-horizontal.css"));
            bundles.Add(new ScriptBundle("~/bundles/timelinejs").Include("~/Scripts/time-line/time-line-horizontal.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            //Estilos Formularios 
            bundles.Add(new StyleBundle("~/Content/ftccss").Include("~/Content/portal-externo/formulario-tarjetas-credito.css"));
            bundles.Add(new StyleBundle("~/Content/frcss").Include("~/Content/portal-externo/formulario-reclamo.css"));

            //Compatibilidad
            bundles.Add(new ScriptBundle("~/bundles/css8").Include("~/Scripts/ie8/css3-mediaqueries.js"));
            bundles.Add(new ScriptBundle("~/bundles/html5shiv-p").Include("~/Scripts/ie8/html5shiv-printshiv.js"));
            bundles.Add(new ScriptBundle("~/bundles/html5shiv").Include("~/Scripts/ie8/html5shiv.js"));
            bundles.Add(new ScriptBundle("~/bundles/html5shiv2").Include("~/Scripts/ie8/html5shiv2.js"));
            //Google
            //bundles.Add(new ScriptBundle("~/bundles/google").Include("~/Scripts/google/google-engine.js"));


            //Canales
            bundles.Add(new ScriptBundle("~/bundles/canalesjs").Include("~/Scripts/portal-externo/Datos.js", "~/Scripts/portal-externo/CanalesBNB.js"));
            bundles.Add(new StyleBundle("~/Content/canalescss").Include("~/Content/portal-externo/Canales.css"));

            BundleTable.EnableOptimizations = false;

        }
    }
}
