﻿using System.Web;
using System.Web.Mvc;

namespace BNB_PortalExterno
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
