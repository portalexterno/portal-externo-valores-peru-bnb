﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Componentes.Entidades
{
    public class Comun
    {
        [Serializable()]
        public abstract class EnvioGenerico
        {
            public string Token { get; set; }
            public string Origen { get; set; }
        }

        public class Response
        {
            //EsValidoResultado
            public bool IsValidResponse { get; internal set; }
            //public string MensajeResultado { get; internal set; }
            public string Message { get; internal set; }
            //public string Codigo { get; set; }
            public string Code { get; set; }
        }

        public enum MessageTypeEnum
        {
            Error = 1,
            Alerta = 2
        }

        public class ResponseMessage
        {
            public bool HasMessage { get; set; }
            public string Message { get; set; }
            public MessageTypeEnum MessageType { get; set; }
        }

        public class ResponseService
        {
            public bool success { get; set; }
            public string message { get; set; }
            public string code { get; set; }
            public string technicalDetail { get; set; }
        }
    }
}