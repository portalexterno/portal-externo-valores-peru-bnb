﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Componentes.Metodos
{
  public class BreadCrumb
  {
    public class ObtenerPorItem
    {
      public class TipoEnvio
      {
        public Models.Menu Item { get; set; }
      }
      public class TipoRespuesta:Models.MensajeRespuesta
      {
        public List<Models.BreadCrumb> BreadCrumb { get; set; }
      }
    }
  }
}