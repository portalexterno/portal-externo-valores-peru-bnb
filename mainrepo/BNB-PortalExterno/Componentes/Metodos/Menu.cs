﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Componentes.Metodos
{
  public class Menu
  {
    public class Obtener
    {
      public class TipoRespuesta : Models.MensajeRespuesta
      {
        public List<Models.Menu> Menu { get; set; }
      }
    }

    public class ObtenerPorId {
      public class TipoEnvio {
        public int Id { get; set; }
      }
      public class TipoRespuesta : Models.MensajeRespuesta {
        public Models.Menu Item { get; set; }
        
      }
    }
  }
}