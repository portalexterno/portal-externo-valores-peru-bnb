﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Componentes.Metodos
{
  public class Parametros
  {
    public class Obtener
    {
      public class TipoRespuesta : Models.MensajeRespuesta
      {
        public List<Models.Parametro> Parametros = new List<Models.Parametro>();
      }
    }
  }
}