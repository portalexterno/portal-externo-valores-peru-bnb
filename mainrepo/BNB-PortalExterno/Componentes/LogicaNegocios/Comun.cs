﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace BNB_PortalExterno.Componentes.LogicaNegocios
{
    public class Comun
    {
        private static readonly Random random = new Random();
        private static readonly object objSincronizacion = new object();
        public static int GenerarCodigoSMS(int min, int max)
        {
            lock (objSincronizacion)
            {
                return random.Next(min, max);
            }
        }
        internal static double ObtenerCargaTope(double ingresoNetoUsd)
        {
            if (ingresoNetoUsd > 5000)
            {
                return 0.50;
            }
            else if (ingresoNetoUsd > 2500)
            {
                return 0.45;
            }
            else if (ingresoNetoUsd > 2000)
            {
                return 0.425;
            }
            else if (ingresoNetoUsd > 1600)
            {
                return 0.4;
            }
            else if (ingresoNetoUsd > 1300)
            {
                return 0.375;
            }
            else if (ingresoNetoUsd > 1100)
            {
                return 0.35;
            }
            else if (ingresoNetoUsd > 900)
            {
                return 0.325;
            }
            else if (ingresoNetoUsd > 700)
            {
                return 0.30;
            }
            else
            {
                return 0.25;
            }
        }
        internal static double CEMConsumoDisponible(double CEMConsumo, double CEMVivienda, double CuotasConsumo, double CuotasVivienda)
        {
            if (CuotasVivienda < (CEMVivienda - CEMConsumo))
            {
                return CEMConsumo - CuotasConsumo;
            }
            else
            {
                return CEMVivienda - CuotasVivienda - CuotasConsumo;
            }
        }

        public static string ConsumirGETRest(string strUrlServicio)
        {
            string strRespuesta = string.Empty;

            var objHttpWebRequest = (HttpWebRequest)WebRequest.Create(strUrlServicio);

            using (var objResponse = objHttpWebRequest.GetResponse())
            {
                using (var objReader = new StreamReader(objResponse.GetResponseStream()))
                {
                    strRespuesta = objReader.ReadToEnd();
                }
            }

            return strRespuesta;
        }
        /// <summary>
        /// Metodo consume servicio rest
        /// </summary>
        /// <param name="strUrlServicio">Url servicio</param>
        /// <param name="strDato">Datos</param>
        /// <returns>
        /// Respuesta
        /// </returns>
        public static string ConsumirPOSTRest(string strUrlServicio, string strDato)
        {
            //solo temporal ----------------------------------------------------
            //WebProxy myproxy = new WebProxy("10.16.202.5", 8080);
            //ICredentials credentials = new NetworkCredential("rochavarria", "sharpey123%");
            //myproxy.Credentials = credentials;
            //------------------------------------------------------------------

            var objRequest = (HttpWebRequest)WebRequest.Create(strUrlServicio);
            var objDatos = Encoding.ASCII.GetBytes(strDato);
            objRequest.Method = "POST";
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.ContentLength = objDatos.Length;
            //objRequest.Proxy = myproxy;
            using (var objStream = objRequest.GetRequestStream())
            {
                objStream.Write(objDatos, 0, objDatos.Length);
            }

            var objRespuesta = (HttpWebResponse)objRequest.GetResponse();
            return new StreamReader(objRespuesta.GetResponseStream()).ReadToEnd();
        }



        public static string CallService(string route, string requestBody)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(route);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            string result = String.Empty;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(requestBody);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        public static string CallGetService(string route)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(route);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            string result = String.Empty;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.WriteLine();
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

    }
}