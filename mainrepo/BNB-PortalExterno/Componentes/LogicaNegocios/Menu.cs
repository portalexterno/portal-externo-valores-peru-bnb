﻿
using BNB_PortalExterno.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Componentes.LogicaNegocios
{
	public class Menu
	{
		/// <summary>
		/// Obtener la estructura del menu
		/// </summary>
		/// <returns></returns>
		public static Metodos.Menu.Obtener.TipoRespuesta Obtener()
		{
			Metodos.Menu.Obtener.TipoRespuesta TipoRespuesta = new Metodos.Menu.Obtener.TipoRespuesta();
			try
			{
				TipoRespuesta.Menu = new List<Models.Menu>();
				//Adicionar datos para el menu principal
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "Index", Area = "#", Controlador = "Principal", Id = 1, IdSuperior = 0, Nombre = "Valores BNB Peru", Orden = 1, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/InicioVerde.png", TipoMenu = Models.Enumeradores.TipoMenu.Principal, EstaSeleccionado = false });
				//TipoRespuesta.Menu.Add(new Models.Menu { Accion = "BancaEmpresas", Area = "#", Controlador = "Principal", Id = 2, IdSuperior = 0, Nombre = "Banca Empresas", Orden = 2, TieneImagen = false, RutaImagen = string.Empty, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/InicioVerde.png", TipoMenu = Models.Enumeradores.TipoMenu.Principal, EstaSeleccionado = false });
				//TipoRespuesta.Menu.Add(new Models.Menu { Accion = "BancaInversiones", Area = "#", Controlador = "Principal", Id = 3, IdSuperior = 0, Nombre = "Banca Inversiones", Orden = 3, TieneImagen = false, RutaImagen = string.Empty, TipoMenu = Models.Enumeradores.TipoMenu.Principal, EstaSeleccionado = false });
				//TipoRespuesta.Menu.Add(new Models.Menu { Accion = "GrupoBNB", Area = "#", Controlador = "Principal", Id = 3, IdSuperior = 0, Nombre = "Grupo BNB", Orden = 4, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/InicioVerde.png", TieneImagen = false, RutaImagen = string.Empty, TipoMenu = Models.Enumeradores.TipoMenu.Principal, EstaSeleccionado = false });
				//Adicionar datos para el Menu de banca personas.
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "Nosotros", Area = "#", Controlador = "ValoresPeru", Id = Constantes.MENU_SECCION_AHORRO_INVERSION, IdSuperior = 1, Nombre = "Nosotros", Orden = 2, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/NosotrosBlanco.png", TipoMenu = Models.Enumeradores.TipoMenu.Menu, TituloBanner = "TENEMOS EL <strong>CRÉDITO PARA TI</strong>", Banner = "~/Images/Banners/nosotros.jpg" });
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "NuestrosServicios", Area = "#", Controlador = "ValoresPeru", IdSuperior = 1, Id = Constantes.MENU_SECCION_CREDITOS, Nombre = "Nuestros<br/>servicios", Orden = 3, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/NuestroServiciosBlanco.png", Banner = "~/Images/Banners/nuestrosServicios.jpg", TipoMenu = Models.Enumeradores.TipoMenu.Menu, Descripcion = "<strong>AQUÍ</strong> EL FORMULARIO DE SOLICITUD DE <br/><strong>PRODUCTO EN LÍNEA</strong>" });
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "RegistroClientes", Area = "#", Controlador = "ValoresPeru", Id = Constantes.MENU_SECCION_TARJETAS_CREDITO, IdSuperior = 1, Nombre = "Registro<br/>de clientes", Orden = 4, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/RegistroClientesBlanco.png", Banner = "~/Images/Banners/registroCliente.jpg", TipoMenu = Models.Enumeradores.TipoMenu.Menu, TituloBanner = "TUS AHORROS E INVERSIONES <strong>SEGUROS</strong>" });
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "Contactos", Area = "#", Controlador = "ValoresPeru", Id = Constantes.MENU_SECCION_SERVICIOS, IdSuperior = 1, Nombre = "Contactos", Orden = 5, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/ContactosBlanco.png", Banner = "~/Images/Banners/contactos.jpg", TipoMenu = Models.Enumeradores.TipoMenu.Menu, TituloBanner = "TENEMOS EL <strong>PRODUCTO PARA TI</strong>" });
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "Noticias", Area = "#", Controlador = "ValoresPeru", Id = Constantes.MENU_SECCION_NOTICIAS, IdSuperior = 1, Nombre = "Noticias", Orden = 6, TieneIcono = true, RutaIcono = "~/Images/IconosSeccion/RegistroClientesBlanco.png", Banner = "~/Images/Banners/RegistroClientesBlanco.jpg", TipoMenu = Models.Enumeradores.TipoMenu.Menu, TituloBanner = "" });

				//  #region Creditos
				//  //Sub menu creditos
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "Naturales", Area = "#", Controlador = "ValoresPeru", Id = 10, IdSuperior = 5, Orden = 1, TieneIcono = false, RutaIcono = "~/Images/IconosSubSeccion/Creditos/CasaBlanco.png", TieneImagen = true, RutaImagen = "~/Images/BannerReducido/naturales.jpg", Nombre = "<strong>Para clientes naturales</strong>", Banner = "~/Images/Banners/naturales.jpg", TituloBanner = string.Empty, Descripcion = "Soluciones diseñadas específicamente para clientes naturales. Intermediacion Bursátil, Custodia, Desmaterialización De Títulos, Asesoría Patrimonial Y De Inversiones", TipoMenu = Models.Enumeradores.TipoMenu.SubMenu, RutaIconoImagen = "~/Images/Iconos/credito-vivienda.png" });
				TipoRespuesta.Menu.Add(new Models.Menu { Accion = "Corporativos", Area = "#", Controlador = "ValoresPeru", Id = 11, IdSuperior = 5, Orden = 2, TieneIcono = false, RutaIcono = "~/Images/IconosSubSeccion/Creditos/AutoBlanco.png", TieneImagen = true, RutaImagen = "~/Images/BannerReducido/corporativos.jpg", Nombre = "<strong>Para clientes corporativos</strong>", Banner = "~/Images/Banners/corporativos.jpg", TituloBanner = string.Empty, Descripcion = "Soluciones diseñadas específicamente para clientes corporativos. Estructuraciones, Finanzas Corporativa, Colocación De Valores, Custodia, Servicio De Representante De Obligacionista, Valorizaciones, Asesoría Financiera e Intermediación Bursátil", TipoMenu = Models.Enumeradores.TipoMenu.SubMenu, RutaIconoImagen = "~/Images/Iconos/credito-vehiculo.png" });
				//  TipoRespuesta.Menu.Add(new Models.Menu { Accion = "MenuConsumo", Area = "#", Controlador = "Creditos", Id = 12, IdSuperior = 5, Orden = 3, TieneIcono = false, RutaIcono = "~/Images/IconosSubSeccion/Creditos/CarteraBlanco.png", TieneImagen = true, RutaImagen = "~/Images/Creditos/BannerReducido/consumo.jpg", Banner = "~/Images/TarjetaCredito/Banners/bg-tj.png", TituloBanner = "TENEMOS EL <strong>CRÉDITO PARA TI </strong>", Nombre = "<strong>.CRÉDITO DE CONSUMO</strong>", Descripcion = "Los créditos de consumo del BNB, destinados a personas naturales, permiten el financiamiento para la compra de bienes y servicios.", TipoMenu = Models.Enumeradores.TipoMenu.SubMenu, RutaIconoImagen = "~/Images/Iconos/credito-consumo.png" });
				//  TipoRespuesta.Menu.Add(new Models.Menu { Accion = "MenuLineaCredito", Area = "#", Controlador = "Creditos", Id = 13, IdSuperior = 5, Orden = 4, TieneIcono = false, RutaIcono = "~/Images/IconosSubSeccion/Creditos/PunoBlanco.png", TieneImagen = true, RutaImagen = "~/Images/Creditos/BannerReducido/linea-credito.jpg", Nombre = "<strong>.LÍNEA DE CRÉDITO</strong>", TituloBanner = "<span class='d40'>LINEA</span><br/><span class='d40'>DE</span>&nbsp;&nbsp;<span class='d60 fuente-bold'>CRÉDITO</span>", Banner = "~/Images/Creditos/Banners/bg-lineacredito-pro.jpg", Descripcion = "La línea de crédito es un acuerdo por medio del cual el banco pone a disposición del cliente un monto específico de dinero, para que pueda realizar diferentes tipos de operaciones crediticias.", TipoMenu = Models.Enumeradores.TipoMenu.SubMenu, RutaIconoImagen = "~/Images/Iconos/linea-credito.png" });
				//  #endregion
				//  #region Items credito
				//  //Vivienda items
				TipoRespuesta.Menu.Add(
					new Models.Menu
					{
						Accion = "NegociacionValores",
						Area = "#",
						Controlador = "ValoresPeru",
						Id = 101,
						IdSuperior = 10,
						Nombre = "<br/><strong>.NEGOCIACIÓN DE VALORES</strong>",
						Orden = 1,
						TieneImagen = true,
						RutaImagen = "~/Images/BannerReducido/negociacionValores.jpg",
						TieneIcono = false,
						RutaIcono = string.Empty,
						TipoMenu = Models.Enumeradores.TipoMenu.item,
						EstaSeleccionado = false,
						Descripcion = "Somos especialistas en el asesoramiento personalizado para la ejecución de órdenes locales y globales en el mercado de capitales, nos enfocamos en una asesoría para un portafolio diversificado con niveles fundamentales y técnicos acorde al nivel de tolerancia al riesgo de cada cliente.",
						Banner = "~/Images/Banners/negociacionValores.jpg",
						TituloBanner = string.Empty
					});

				TipoRespuesta.Menu.Add(
					new Models.Menu
					{
						Accion = "CustorioDesmaterializacion",
						Area = "#",
						Controlador = "ValoresPeru",
						Id = 102,
						IdSuperior = 10,
						Nombre = "<br/><strong>.CUSTODIA Y DESMATERIALIZACIÓN DE TÍTULOS</strong>",
						Orden = 1,
						TieneImagen = true,
						RutaImagen = "~/Images/BannerReducido/custorioDesmaterializacion.jpg",
						TipoMenu = Models.Enumeradores.TipoMenu.item,
						Descripcion = "La custodia de valores es una actividad mediante la cual BNB Valores Peru recibe en depósito, títulos representativos de valores de oferta pública y/o oferta privada, para su resguardo, cuidado y mantención.",
						EstaSeleccionado = false,
						TieneIcono = false,
						RutaIcono = string.Empty,
						Banner = "~/Images/Banners/custorioDesmaterializacion.jpg",
						TituloBanner = string.Empty
					});

				TipoRespuesta.Menu.Add(
			 new Models.Menu
			 {
				 Accion = "AsesoriaInversiones",
				 Area = "#",
				 Controlador = "ValoresPeru",
				 Id = 103,
				 IdSuperior = 10,
				 Nombre = "<br/><strong>.ASESORÍA DE INVERSIONES</strong>",
				 Orden = 1,
				 TieneImagen = true,
				 RutaImagen = "~/Images/BannerReducido/asesoriaInversiones.jpg",
				 TipoMenu = Models.Enumeradores.TipoMenu.item,
				 Descripcion = "Servicio que consiste en el asesoramiento a nuestros clientes para el planeamiento de estrategias financieras enfocadas en incrementar  el valor de su portafolio, acorde su apetito de riesgo y horizonte de inversión.",
				 EstaSeleccionado = false,
				 TieneIcono = false,
				 RutaIcono = string.Empty,
				 Banner = "~/Images/Banners/asesoriaInversiones.jpg",
				 TituloBanner = string.Empty
			 });

				TipoRespuesta.Menu.Add(
				new Models.Menu
				{
					Accion = "AsesoriaPatrimonial",
					Area = "#",
					Controlador = "ValoresPeru",
					Id = 104,
					IdSuperior = 10,
					Nombre = "<br/><strong>.ASESORÍA PATRIMONIAL</strong>",
					Orden = 1,
					TieneImagen = true,
					RutaImagen = "~/Images/BannerReducido/AsesoriaPatrimonial.jpg",
					TipoMenu = Models.Enumeradores.TipoMenu.item,
					Descripcion = "Servicio que consiste en el asesoramiento a nuestros clientes para el planeamiento de estrategias financieras enfocadas en incrementar  el valor de su portafolio, acorde su apetito de riesgo y horizonte de inversión.",
					EstaSeleccionado = false,
					TieneIcono = false,
					RutaIcono = string.Empty,
					Banner = "~/Images/Banners/AsesoriaPatrimonial.jpg",
					TituloBanner = string.Empty
				});

				TipoRespuesta.Menu.Add(
					new Models.Menu
					{
						Accion = "Estructuraciones",
						Area = "#",
						Controlador = "ValoresPeru",
						IdSuperior = 11,
						Id = 121,
						Nombre = "<strong>.ESTRUCTURACIONES Y FINANZAS CORPORATIVAS</strong>",
						Orden = 1,
						TieneImagen = true,
						RutaImagen = "~/Images/BannerReducido/estructuraciones.jpg",
						TieneIcono = false,
						RutaIcono = string.Empty,
						Banner = "~/Images/Banners/estructuraciones.jpg",
						Descripcion = "Servicio dirigido a empresas que requieran maximizar su potencial estratégico y aumentar su valor económico. BNB Valores Peru busca entender  las necesidades financieras específicas de cada cliente para lo cual desarrollamos estrategias específicas e integrales.",
						TipoMenu = Models.Enumeradores.TipoMenu.item,
						TituloBanner = string.Empty
					});

				TipoRespuesta.Menu.Add(
					new Models.Menu
					{
						Accion = "Colocaciones",
						Area = "#",
						Controlador = "ValoresPeru",
						IdSuperior = 11,
						Id = 122,
						Nombre = "<strong>.COLOCACIÓN DE VALORES</strong>",
						Orden = 1,
						TieneImagen = true,
						RutaImagen = "~/Images/BannerReducido/colocacion.jpg",
						TieneIcono = true,
						RutaIcono = string.Empty,
						Banner = "~/Images/Banners/colocacion.jpg",
						Descripcion = "La colocación de los valores consiste en la venta en el mercado primario ya sea público o privado de los títulos representativos de deuda o de capital, con la finalidad de obtener los recursos que el emisor requiere para el desarrollo de su negocio.",
						TipoMenu = Models.Enumeradores.TipoMenu.item,
						TituloBanner = string.Empty
					});

				TipoRespuesta.Menu.Add(
					new Models.Menu
					{
						Accion = "CustodiaValores",
						Area = "#",
						Controlador = "ValoresPeru",
						IdSuperior = 11,
						Id = 123,
						Nombre = "<strong>.CUSTODIA DE VALORES</strong>",
						Orden = 1,
						TieneImagen = true,
						RutaImagen = "~/Images/BannerReducido/custodia.jpg",
						TieneIcono = false,
						RutaIcono = string.Empty,
						Banner = "~/Images/Banners/custodia.jpg",
						Descripcion = "La custodia de valores es una actividad mediante la cual BNB Valores Peru recibe en depósito, títulos representativos de valores de oferta pública y/o oferta privada, para su resguardo, cuidado y mantención.",
						TipoMenu = Models.Enumeradores.TipoMenu.item,
						TituloBanner = string.Empty
					});

				TipoRespuesta.Menu.Add(
					new Models.Menu
					{
						Accion = "ServicioRepresentante",
						Area = "#",
						Controlador = "ValoresPeru",
						IdSuperior = 11,
						Id = 124,
						Nombre = "<strong>.SERVICIO DE REPRESENTANTE DE OBLIGACIONISTAS</strong>",
						Orden = 1,
						TieneImagen = true,
						RutaImagen = "~/Images/BannerReducido/servicioRepresentante.jpg",
						TieneIcono = false,
						RutaIcono = string.Empty,
						Banner = "~/Images/Banners/servicioRepresentante.jpg",
						Descripcion = "Servicio post inversión que consistente en la representación del Sindicato de tenedores u obligacionistas de títulos de deuda emitidos en el marco de un programa de emisión de valores",
						TipoMenu = Models.Enumeradores.TipoMenu.item,
						TituloBanner = string.Empty
					});
				TipoRespuesta.Menu.Add(
					new Models.Menu
					{
						Accion = "ValorizacionesAsesoria",
						Area = "#",
						Controlador = "ValoresPeru",
						IdSuperior = 11,
						Id = 125,
						Nombre = "<strong>.VALORIZACIONES Y ASESORIA FINANCIERA</strong>",
						Orden = 1,
						TieneImagen = true,
						RutaImagen = "~/Images/BannerReducido/valorizaciones.jpg",
						TieneIcono = false,
						RutaIcono = string.Empty,
						Banner = "~/Images/Banners/valorizaciones.jpg",
						Descripcion = "BNB Valores brinda el servicio de asesoría financiera enfocadas en buscar soluciones holísticas con estrategias financieras para nuestros clientes, estos incluyen la valorización de empresas.",
						TipoMenu = Models.Enumeradores.TipoMenu.item,
						TituloBanner = string.Empty
					});
				
				TipoRespuesta.Menu.Add(
				new Models.Menu
				{
					Accion = "NegociacionValoresCorp",
					Area = "#",
					Controlador = "ValoresPeru",
					IdSuperior = 11,
					Id = 126,
					Nombre = "<strong>.NEGOCIACIÓN DE VALORES</strong>",
					Orden = 1,
					TieneImagen = true,
					RutaImagen = "~/Images/BannerReducido/negociacionValoresCorp.jpg",
					TieneIcono = false,
					RutaIcono = string.Empty,
					Banner = "~/Images/Banners/negociacionValoresCorp.jpg",
					Descripcion = "Somos especialistas en el asesoramiento personalizado para la ejecución de órdenes locales y globales en el mercado de capitales, nos enfocamos en una asesoría para un portafolio diversificado con niveles fundamentales y técnicos acorde al nivel de tolerancia al riesgo de cada cliente.",
					TipoMenu = Models.Enumeradores.TipoMenu.item,
					TituloBanner = string.Empty
				});				

			}
			catch (Exception ex)
			{
				TipoRespuesta.Descripcion = ex.StackTrace;
				TipoRespuesta.Mensaje = ex.Message;
				TipoRespuesta.OcurrioError = true;
				TipoRespuesta.TipoError = Models.Enumeradores.TipoError.Error;
			}
			return TipoRespuesta;
		}

		public static Metodos.Menu.ObtenerPorId.TipoRespuesta ObtenerPorId(Metodos.Menu.ObtenerPorId.TipoEnvio TipoEnvio)
		{
			Metodos.Menu.ObtenerPorId.TipoRespuesta TipoRespuesta = new Metodos.Menu.ObtenerPorId.TipoRespuesta();
			try
			{
				//Obtener la informacion del item, item dependientes, y el breadcrumb desde la lista del menu
				if (HttpContext.Current.Application[Models.Constantes.MENU] != null)
				{
					List<Models.Menu> Menu = HttpContext.Current.Application[Models.Constantes.MENU] as List<Models.Menu>;
					//Obtener el item
					if (Menu.Exists(x => x.Id == TipoEnvio.Id))
					{
						TipoRespuesta.Item = new Models.Menu();
						TipoRespuesta.Item = Menu.Find(x => x.Id == TipoEnvio.Id);
						//Obtener la informacion de los items dependientes.
						if (Menu.Exists(x => x.IdSuperior == TipoRespuesta.Item.Id))
						{
							TipoRespuesta.Item.ItemMenu = new List<Models.Menu>();
							TipoRespuesta.Item.ItemMenu = Menu.FindAll(x => x.IdSuperior == TipoRespuesta.Item.Id);
						}
						//Obtener el bread-crumb del item
						Metodos.BreadCrumb.ObtenerPorItem.TipoEnvio TipoEnvioObtenerBreadCrumb = new Metodos.BreadCrumb.ObtenerPorItem.TipoEnvio();
						TipoEnvioObtenerBreadCrumb.Item = new Models.Menu();
						TipoEnvioObtenerBreadCrumb.Item = TipoRespuesta.Item;
						Metodos.BreadCrumb.ObtenerPorItem.TipoRespuesta TipoRespuestaObtenerBreadCrumb = BreadCrumb.ObtenerPorItem(TipoEnvioObtenerBreadCrumb);
						if (!TipoRespuestaObtenerBreadCrumb.OcurrioError)
						{
							TipoRespuesta.Item.BreadCrumbs = new List<Models.BreadCrumb>();
							TipoRespuesta.Item.BreadCrumbs = TipoRespuestaObtenerBreadCrumb.BreadCrumb;
						}
						else
						{
							TipoRespuesta.OcurrioError = TipoRespuestaObtenerBreadCrumb.OcurrioError;
							TipoRespuesta.Mensaje = TipoRespuestaObtenerBreadCrumb.Mensaje;
							TipoRespuesta.TipoError = TipoRespuestaObtenerBreadCrumb.TipoError;
							TipoRespuesta.Descripcion = TipoRespuestaObtenerBreadCrumb.Descripcion;
						}
					}
					//Obtener el detalle del menu item
					List<Models.DetalleItemMenu> DetalleItemMenu = HttpContext.Current.Application[Models.Constantes.DETALLES_ITEM_MENU] as List<Models.DetalleItemMenu>;
					if (DetalleItemMenu.FindAll(x => x.IdItemMenu == TipoRespuesta.Item.Id).Count > 0)
					{
						TipoRespuesta.Item.DetalleMenuItem = new List<Models.DetalleItemMenu>();
						TipoRespuesta.Item.DetalleMenuItem = DetalleItemMenu.FindAll(x => x.IdItemMenu == TipoRespuesta.Item.Id);
					}
				}
				else
				{
					//TODO GENERAR MENSAJE DE ALERTA
				}

			}
			catch (Exception ex)
			{
				TipoRespuesta.OcurrioError = true;
				TipoRespuesta.Mensaje = ex.Message;
				TipoRespuesta.Descripcion = ex.StackTrace;
				TipoRespuesta.TipoError = Models.Enumeradores.TipoError.Error;
			}

			return TipoRespuesta;
		}

	}
}