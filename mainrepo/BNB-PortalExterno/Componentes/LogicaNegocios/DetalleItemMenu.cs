﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Componentes.LogicaNegocios
{
	public class DetalleItemMenu
	{
		public static Metodos.DetalleItemMenu.Obtener.TipoRespuesta Obtener()
		{
			Metodos.DetalleItemMenu.Obtener.TipoRespuesta TipoRespuesta = new Metodos.DetalleItemMenu.Obtener.TipoRespuesta();
			try
			{
				//Generar la informacion del detalle de los items.
				
				//Detalle de crédito vivienda compra
				Models.DetalleItemMenu DetalleItemMenuCreditoViviendaCompra = new Models.DetalleItemMenu();
				DetalleItemMenuCreditoViviendaCompra.Id = 1;
				DetalleItemMenuCreditoViviendaCompra.IdItemMenu = Models.Constantes.ITEM_NEGOCIACION_VALORES;
				DetalleItemMenuCreditoViviendaCompra.Encabezado = "Negociación de Valores";
				DetalleItemMenuCreditoViviendaCompra.Descripcion = "Somos especialistas en el asesoramiento personalizado para la ejecución de órdenes locales y globales en el mercado de capitales, nos enfocamos en una asesoría para un portafolio diversificado con niveles fundamentales y técnicos acorde al nivel de tolerancia al riesgo de cada cliente.";
				DetalleItemMenuCreditoViviendaCompra.Beneficios = new List<string>();
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Ejecución Diligente");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Mesa de Trading a disposición del cliente durante el horario de negociación en Bolsa de Valores de Lima");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Comisiones competitivas a nivel del mercado");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Inmediata respuesta en ejecución de órdenes");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Ejecución de Órdenes Locales");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Acceso a las operaciones de renta fija y renta variable");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Todas las posiciones de los clientes quedan custodiadas en BNB Valores Perú a través de CAVALI");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Ejecución de Órdenes Extranjeras");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Acceso a las operaciones de renta fija y renta variable");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Acceso a principales mercados del mundo");
				DetalleItemMenuCreditoViviendaCompra.Beneficios.Add("Liquidaciones en Cuenta Global, DVP");
				DetalleItemMenuCreditoViviendaCompra.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemMenuCreditoViviendaCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Renta Variable", Contenidos = new List<string> { "Acciones listadas en la Bolsa de Valores de Lima (BVL)", "Acciones Extranjeras listadas en la BVL y listadas en Bolsa Extranjera.", "Exchange Traded Funds, listados en la BVL " } });
				DetalleItemMenuCreditoViviendaCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Renta Fija", Contenidos = new List<string> { "Papeles Comerciales", "Bonos de Arrendamiento Financiero.", "Bonos Corporativos.", "Bonos de Titulización.", "Bonos Subordinados.", "Bonos Soberanos.", "Bonos Estructurados.", "Operaciones de Reporte", "Letras del Tesoro Público.", "Facturas Negociables" } });
				DetalleItemMenuCreditoViviendaCompra.Requisitos = new List<string>();
				DetalleItemMenuCreditoViviendaCompra.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú");
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemMenuCreditoViviendaCompra);

				//Detalle de crédito vivienda Social compra
				//Models.DetalleItemMenu DetalleItemMenuCreditoViviendaSocialCompra = new Models.DetalleItemMenu();
				//DetalleItemMenuCreditoViviendaSocialCompra.Id = 3;
				//DetalleItemMenuCreditoViviendaSocialCompra.IdItemMenu = Models.Constantes.ITEM_CREDITO_VIVIENDA_SOCIAL;
				//DetalleItemMenuCreditoViviendaSocialCompra.Encabezado = "Crédito de Vivienda Social<br /><strong>Compra</strong>";
				//DetalleItemMenuCreditoViviendaSocialCompra.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				//DetalleItemMenuCreditoViviendaSocialCompra.Descripcion = "Préstamo cuyo destino sea la compra de vivienda o terreno para construcción de una Vivienda Social";
				//DetalleItemMenuCreditoViviendaSocialCompra.Beneficios = new List<string>();
				//DetalleItemMenuCreditoViviendaSocialCompra.Beneficios.Add("Pre pagos a capital permitidos en todo momento.");
				//DetalleItemMenuCreditoViviendaSocialCompra.Beneficios.Add("Débito automático para pago de amortizaciones.");
				//DetalleItemMenuCreditoViviendaSocialCompra.Beneficios.Add("Financiamiento hasta el 100 %.");
				//DetalleItemMenuCreditoViviendaSocialCompra.Beneficios.Add("Hasta 30 años plazos");
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas = new List<Models.TipoLista>();
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Moneda", Contenidos = new List<string> { "Crédito en moneda nacional" } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Tipo de amortización", Contenidos = new List<string> { "Cuota fija Mensual a capital e intereses" } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Monto Máximo a Financiar", Contenidos = new List<string> { "100% s/g valor total del proyecto o compra/venta (No se admiten excepciones), para Compra o Construcción de vivienda social con garantía del Fondo de Depósitos de Garantías del BNB." } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Plazo máximo", Contenidos = new List<string> { "Compra vivienda: Hasta 20 años", "Compra de terreno: Hasta 10 años" } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Desembolso", Contenidos = new List<string> { "Compra Vivienda o Terreno: Uno solo " } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Garantía", Contenidos = new List<string> { "Del Bien a adquirirse (inmueble o terreno)" } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Seguros", Contenidos = new List<string> { "Desgravamen", "Incendio, Aliados y otros seguros para el bien otorgado en garantía" } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Otros Pagos", Contenidos = new List<string> { "Avalúo por cuenta del cliente.", "Costos de transferencia e inscripción por cuenta del cliente" } });
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos = new List<string>();
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Fotocopia de cédula de identidad de titular, cónyuge y garantes(si aplica).");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Fuente de pago: Ingresos familiares (sueldos y salarios, utilidades y / u otro ingresos respaldados).");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Estabilidad laboral mínima un (1) año.");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Carga financiera total / renta total: Según manual de políticas de crédito vigentes.");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Documentación de respaldo de deudas vigentes en el sistema financiero del titular, cónyuge y garantes(si aplica).");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Documentación de respaldo de cajas de ahorro o cuentas corrientes del titular, cónyuge y garantes(si aplica).");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Llenado de formulario de Solicitud de Crédito.");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Llenado de formulario de Declaración Patrimonial(adjuntar respaldo de activos declarados).");
				//DetalleItemMenuCreditoViviendaSocialCompra.Requisitos.Add("Contrato de Anticrético previamente firmado.");
				//TipoRespuesta.DetalleItemsMenu.Add(DetalleItemMenuCreditoViviendaSocialCompra);

				Models.DetalleItemMenu DetalleItemCustodioDesmaterializacion = new Models.DetalleItemMenu();
				DetalleItemCustodioDesmaterializacion.Id = 3;
				DetalleItemCustodioDesmaterializacion.IdItemMenu = Models.Constantes.ITEM_CUSTODIO_DESMATERIALIZACION;
				DetalleItemCustodioDesmaterializacion.Encabezado = "Custodio y desmaterialización de titulos";
				DetalleItemCustodioDesmaterializacion.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemCustodioDesmaterializacion.Descripcion = "La custodia de valores es una actividad mediante la cual BNB Valores Peru recibe en depósito, títulos representativos de valores de oferta pública y/o oferta privada, para su resguardo, cuidado y mantención.";
				DetalleItemCustodioDesmaterializacion.Beneficios = new List<string>();
				DetalleItemCustodioDesmaterializacion.Beneficios.Add("Transparencia y seguridad en liquidaciones de valores.");
				DetalleItemCustodioDesmaterializacion.Beneficios.Add("Disminuir los costos de Custodia y Transporte de Valores.");
				DetalleItemCustodioDesmaterializacion.Beneficios.Add("Disminuir los riesgos del traslado de valores.");
				DetalleItemCustodioDesmaterializacion.Beneficios.Add("Aumentar la capacidad de Negocios.");
				DetalleItemCustodioDesmaterializacion.Beneficios.Add("Facilitar la cobranza de los derechos patrimoniales de los Valores.");
				DetalleItemCustodioDesmaterializacion.Beneficios.Add("Mejor información y transparencia y responder por la autenticidad de los Valores.");
				DetalleItemCustodioDesmaterializacion.Beneficios.Add("Disminución de costos en la gestión de la custodia.");
				DetalleItemCustodioDesmaterializacion.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemCustodioDesmaterializacion.Caracteristicas.Add(new Models.TipoLista { Encabezado = "El Servicio de Custodio incluyen", Contenidos = new List<string> { "Custodia Física y/o Anotación en cuenta de Valores", "Reportes mensuales de Estados de Cuenta y su valorización", "Depósito y Retiro de Títulos", "Cobro de dividendos, acciones liberadas y cupones.", "Registro Electrónico de Valores", "Registro de Compras y Ventas", "Transferencia de Valores", "Certificados de Custodia" } });
				DetalleItemCustodioDesmaterializacion.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemCustodioDesmaterializacion);
				DetalleItemCustodioDesmaterializacion.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");
				DetalleItemCustodioDesmaterializacion.Requisitos.Add("Firmar el contrato de custodia");

				Models.DetalleItemMenu DetalleItemAsesoriaInversiones = new Models.DetalleItemMenu();
				DetalleItemAsesoriaInversiones.Id = 4;
				DetalleItemAsesoriaInversiones.IdItemMenu = Models.Constantes.ITEM_ASESORIA_INVERSIONES;
				DetalleItemAsesoriaInversiones.Encabezado = "Asesoria Inversiones";
				DetalleItemAsesoriaInversiones.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemAsesoriaInversiones.Descripcion = "Servicio que consiste en el asesoramiento a nuestros clientes para el planeamiento de estrategias financieras enfocadas en incrementar  el valor de su portafolio, acorde su apetito de riesgo y horizonte de inversión.";
				DetalleItemAsesoriaInversiones.Beneficios = new List<string>();
				DetalleItemAsesoriaInversiones.Beneficios.Add("Aumento del valor patrimonial");
				DetalleItemAsesoriaInversiones.Beneficios.Add("Diversificación del portafolio");
				DetalleItemAsesoriaInversiones.Beneficios.Add("Confianza y credibilidad en BNB Valores Perú");
				DetalleItemAsesoriaInversiones.Beneficios.Add("Ejecución de órdenes a través de Representantes Bursátiles de experiencia");
				DetalleItemAsesoriaInversiones.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemAsesoriaInversiones.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Definición de Objetivos", Contenidos = new List<string> { "Medimos el Perfil de Riesgo de nuestros clientes", "Consideramos el horizonte de inversión", "Evaluamos estrategias ante necesidades de liquidez" } });
				DetalleItemAsesoriaInversiones.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Estrategias de Inversión", Contenidos = new List<string> { "Nuestros representantes bursátiles constantemente monitorean las oportunidades de inversión", "Tienen un abanico de posibilidades con criterios de diversificación", "Asignación de activos" } });
				DetalleItemAsesoriaInversiones.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Implementación de Portafolio", Contenidos = new List<string> { "Ejecución del Asset Allocation del cliente", "Inversiones por tipo de mercado (Local – Extranjero)", "Inversiones por intrumento (renta fija, renta variable)" } });
				DetalleItemAsesoriaInversiones.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Seguimiento y Redefinición", Contenidos = new List<string> { "Revisión de Portafolio y alternativas", "Evaluación de las nuevas perspectivas del mercado", "Cobranza de cupones, vencimientos y dividendos." } });
				DetalleItemAsesoriaInversiones.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemAsesoriaInversiones);
				DetalleItemAsesoriaInversiones.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");


				Models.DetalleItemMenu DetalleItemAsesoriaPatrimonio = new Models.DetalleItemMenu();
				DetalleItemAsesoriaPatrimonio.Id = 5;
				DetalleItemAsesoriaPatrimonio.IdItemMenu = Models.Constantes.ITEM_ASESORIA_PATRIMONIAL;
				DetalleItemAsesoriaPatrimonio.Encabezado = "Asesoria Patrimonial";
				DetalleItemAsesoriaPatrimonio.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemAsesoriaPatrimonio.Descripcion = "Servicio que consiste en el asesoramiento a nuestros clientes para el planeamiento de estrategias financieras enfocadas en incrementar  el valor de su portafolio, acorde su apetito de riesgo y horizonte de inversión.";
				DetalleItemAsesoriaPatrimonio.Beneficios = new List<string>();
				DetalleItemAsesoriaPatrimonio.Beneficios.Add("Aumento del valor patrimonial");
				DetalleItemAsesoriaPatrimonio.Beneficios.Add("Diversificación del portafolio");
				DetalleItemAsesoriaPatrimonio.Beneficios.Add("Confianza y credibilidad en BNB Valores Perú");
				DetalleItemAsesoriaPatrimonio.Beneficios.Add("Ejecución de órdenes a través de Representantes Bursátiles de experiencia");
				DetalleItemAsesoriaPatrimonio.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemAsesoriaPatrimonio.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Definición de Objetivos", Contenidos = new List<string> { "Medimos el Perfil de Riesgo de nuestros clientes", "Consideramos el horizonte de inversión", "Evaluamos estrategias ante necesidades de liquidez" } });
				DetalleItemAsesoriaPatrimonio.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Estrategias de Inversión", Contenidos = new List<string> { "Nuestros representantes bursátiles constantemente monitorean las oportunidades de inversión", "Tienen un abanico de posibilidades con criterios de diversificación", "Asignación de activos" } });
				DetalleItemAsesoriaPatrimonio.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Implementación de Portafolio", Contenidos = new List<string> { "Ejecución del Asset Allocation del cliente", "Inversiones por tipo de mercado (Local – Extranjero)", "Inversiones por intrumento (renta fija, renta variable)" } });
				DetalleItemAsesoriaPatrimonio.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Seguimiento y Redefinición", Contenidos = new List<string> { "Revisión de Portafolio y alternativas", "Evaluación de las nuevas perspectivas del mercado", "Cobranza de cupones, vencimientos y dividendos." } });
				DetalleItemAsesoriaPatrimonio.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemAsesoriaPatrimonio);
				DetalleItemAsesoriaPatrimonio.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");



				Models.DetalleItemMenu DetalleItemEstructuraciones = new Models.DetalleItemMenu();
				DetalleItemEstructuraciones.Id = 121;
				DetalleItemEstructuraciones.IdItemMenu = Models.Constantes.ITEM_ESTRUCTURACIONES;
				DetalleItemEstructuraciones.Encabezado = "Estructuraciones y finanzas corporativas";
				DetalleItemEstructuraciones.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemEstructuraciones.Descripcion = "Servicio dirigido a empresas que requieran maximizar su potencial estratégico y aumentar su valor económico. BNB Valores Peru busca entender las necesidades financieras específicas de cada cliente para lo cual desarrollamos estrategias específicas e integrales. Es por eso que ofrecemos soluciones innovadoras mediante metodologías de trabajo desarrolladas a según la necesidad  de cada contexto económico y de negocios.";
				DetalleItemEstructuraciones.Beneficios = new List<string>();
				DetalleItemEstructuraciones.Beneficios.Add("Líderes en la estructuración y colocación de instrumentos de Deuda en el Marcado Alternativo de Valores");
				DetalleItemEstructuraciones.Beneficios.Add("Más de 10 años de Experiencia con clientes corporativos, empresas medianas y grandes.");
				DetalleItemEstructuraciones.Beneficios.Add("Relaciones con los principales inversionistas institucionales, fondos de inversión y banca privada");
				DetalleItemEstructuraciones.Beneficios.Add("Asesoría permanente y sostenible en beneficio y crecimiento del cliente.");
				DetalleItemEstructuraciones.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemEstructuraciones.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Caracteristicas", Contenidos = new List<string> { "Estructuración de programas de emisión de valores en loa diferentes segmentos del mercado bursátil", "Reestructuración de pasivos financieros", "Obtención de fuentes alternativas de financiamiento", "Fusiones y adquisiciones", "Financiamiento de Proyectos (Project Finance)" } });
				DetalleItemEstructuraciones.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemEstructuraciones);
				DetalleItemEstructuraciones.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");
				DetalleItemEstructuraciones.Requisitos.Add("Firma del contrato mandato de Asesoría Financiera");

				Models.DetalleItemMenu DetalleItemColocacion = new Models.DetalleItemMenu();
				DetalleItemColocacion.Id = 122;
				DetalleItemColocacion.IdItemMenu = Models.Constantes.ITEM_COLOCACION_VALORES;
				DetalleItemColocacion.Encabezado = "Colocación de Valores";
				DetalleItemColocacion.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemColocacion.Descripcion = "La colocación de los valores consiste en la venta en el mercado primario ya sea público o privado de los títulos representativos de deuda o de capital, con la finalidad de obtener los recursos que el emisor requiere para el desarrollo de su negocio.";
				DetalleItemColocacion.Beneficios = new List<string>();
				DetalleItemColocacion.Beneficios.Add("Amplia experiencia de BNB Valores Peru en la colocación de instrumentos de deuda con más de 450 millones de soles levantados en la Bolsa de Valores de Lima.");
				DetalleItemColocacion.Beneficios.Add("Identificación y búsqueda de Inversionistas potenciales, manejamos relaciones con importantes inversionistas institucionales, fondos de inversión y Banca Privada.");
				DetalleItemColocacion.Beneficios.Add("Acceso a la plataforma de negociación de la Bolsa de Valores de Lima para colocar los instrumentos financieros por oferta pública.");
				DetalleItemColocacion.Beneficios.Add("Contamos con toda la infraestructura de negociación y de back office necesario, para que la colocación de valores sea eficiente y eficaz.");
				DetalleItemColocacion.Beneficios.Add("Experiencia en colocaciones privadas.");
				//DetalleItemEstructuraciones.Caracteristicas = new List<Models.TipoLista>();
				//DetalleItemEstructuraciones.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Caracteristicas", Contenidos = new List<string> { "Estructuración de programas de emisión de valores en loa diferentes segmentos del mercado bursátil", "Reestructuración de pasivos financieros", "Obtención de fuentes alternativas de financiamiento", "Fusiones y adquisiciones", "Financiamiento de Proyectos (Project Finance)" } });
				DetalleItemColocacion.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemColocacion);
				DetalleItemColocacion.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");
				DetalleItemColocacion.Requisitos.Add("Firmar contrato de Agente Colocador");

				Models.DetalleItemMenu DetalleItemCustodia = new Models.DetalleItemMenu();
				DetalleItemCustodia.Id = 123;
				DetalleItemCustodia.IdItemMenu = Models.Constantes.ITEM_CUSTODIA_VALORES;
				DetalleItemCustodia.Encabezado = "Custodia de valores";
				DetalleItemCustodia.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemCustodia.Descripcion = "La custodia de valores es una actividad mediante la cual BNB Valores Peru recibe en depósito, títulos representativos de valores de oferta pública y/o oferta privada, para su resguardo, cuidado y mantención.";
				DetalleItemCustodia.Beneficios = new List<string>();
				DetalleItemCustodia.Beneficios.Add("Transparencia y seguridad en liquidaciones de valores");
				DetalleItemCustodia.Beneficios.Add("Disminuir los costos de Custodia y Transporte de Valores");
				DetalleItemCustodia.Beneficios.Add("Disminuir los riesgos del traslado de valores");
				DetalleItemCustodia.Beneficios.Add("Aumentar la capacidad de Negocios.");
				DetalleItemCustodia.Beneficios.Add("Facilitar la cobranza de los derechos patrimoniales de los Valores.");
				DetalleItemCustodia.Beneficios.Add("Mejor información y transparencia y responder por la autenticidad de los Valores.");
				DetalleItemCustodia.Beneficios.Add("Disminución de costos en la gestión de la custodia");
				DetalleItemCustodia.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemCustodia.Caracteristicas.Add(new Models.TipoLista { Encabezado = "El Servicio de Custodio incluyen", Contenidos = new List<string> { "Custodia Física y/o Anotación en cuenta de Valores", "Reportes mensuales de Estados de Cuenta y su valorización", "Depósito y Retiro de Títulos", "Cobro de dividendos, acciones liberadas y cupones.", "Registro Electrónico de Valores", "Registro de Compras y Ventas", "Transferencia de Valores", "Certificados de Custodia" } });
				DetalleItemCustodia.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemCustodia);
				DetalleItemCustodia.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");
				DetalleItemCustodia.Requisitos.Add("Firmar el contrato de custodia");


				Models.DetalleItemMenu DetalleItemServicio = new Models.DetalleItemMenu();
				DetalleItemServicio.Id = 124;
				DetalleItemServicio.IdItemMenu = Models.Constantes.ITEM_SERVICIO_REPRESENTANTE;
				DetalleItemServicio.Encabezado = "Servicio de representante";
				DetalleItemServicio.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemServicio.Descripcion = "Servicio post inversión que consistente en la representación del Sindicato de tenedores u obligacionistas de títulos de deuda emitidos en el marco de un programa de emisión de valores.";
				DetalleItemServicio.Beneficios = new List<string>();
				DetalleItemServicio.Beneficios.Add("Experiencia en emisiones con aproximadamente 42 contratos en emisiones por más de US$137 millones (Ciento Treinta y siete y 00/100 millones de dólares) en colocaciones.");
				DetalleItemServicio.Beneficios.Add("Reducción de carga Operativa");
				DetalleItemServicio.Beneficios.Add("Nuestra oferta consiste en un servicio transparente y diligente");
				DetalleItemServicio.Beneficios.Add("Control y seguimiento de las inversiones realizadas.");
				DetalleItemServicio.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemServicio.Caracteristicas.Add(new Models.TipoLista
				{
					Encabezado = "Responsabilidades",
					Contenidos = new List<string> {  "Realizar todas las revisiones y coordinaciones de los Contratos del Programa de los Instrumentos financieros",
																																																																					"Realizar todas las coordinaciones pertinentes con el Estructurador y Colocador.",
																																																																					"Vigilar el pago de los intereses y del principal de los Instrumentos financieros, y en general, cautelar todos los derechos de los Bonistas",
																																																																					"Velar por el cumplimiento de los compromisos asumidos por el emisor frente a  los obligacionistas.",
						"Presidir la Asamblea de Obligacionistas de la Emisión",
						"Ejercer la Representación legal del Sindicato de Obligacionistas de la Emisión.",
						"Designar a la persona natural que lo representará permanentemente  ante la sociedad emisora en sus funciones de Representante de los Obligacionistas.",
						"Designar a una persona natural para que forme parte del órgano administrador de la sociedad emisora, cuando la participación de un representante de los obligacionistas en dicho directorio estuviese prevista en la escritura pública de emisión.",
						"Convocar a la junta de accionistas o socios, según sea el caso, de la sociedad emisora si ocurriese un atraso en el pago de los intereses vencidos o en la amortización del principal.",
						"Convocar a la Asamblea de Obligacionistas, en caso el Emisor  no la convoque.",
						"Exigir y supervisar el cumplimiento de los convenants y demás obligaciones asumidos por el Emisor en el marco de la Emisión.",
						"Cuidar que los bienes dados en garantía se encuentren, de acuerdo a su naturaleza, debidamente asegurados a favor del Representante de Obligacionistas, en representación de los obligacionistas, al menos por un monto equivalente al importe garantizado.",
						"Verificar que las garantías de la Emisión hayan sido debidamente  constituidas y sean exigibles.",
						"Exigir y supervisar el cumplimiento de los convenants y demás obligaciones asumidos por el Emisor en el marco de la Emisión.",
						"Iniciar y proseguir las pretensiones judiciales y extrajudiciales, en especial las que tengan por objeto del pago de los intereses y del capital adeudados, la ejecución de las garantías, entre otros.",
				}
				});
				DetalleItemServicio.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemServicio);
				DetalleItemServicio.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");
				DetalleItemServicio.Requisitos.Add("Contrato de Servicio de Representante de Obligacionistas.");


				Models.DetalleItemMenu DetalleItemValorizacion = new Models.DetalleItemMenu();
				DetalleItemValorizacion.Id = 125;
				DetalleItemValorizacion.IdItemMenu = Models.Constantes.ITEM_VALORIZACIONES_ASESORIA;
				DetalleItemValorizacion.Encabezado = "Valorizaciones y asesoria financiera";
				DetalleItemValorizacion.TipoDetalle = Models.Enumeradores.TipoDetalle.Compra;
				DetalleItemValorizacion.Descripcion = "BNB Valores brinda el servicio de asesoría financiera enfocadas en buscar soluciones holísticas con estrategias financieras para nuestros clientes, estos incluyen la valorización de empresas.";
				DetalleItemValorizacion.Beneficios = new List<string>();
				DetalleItemValorizacion.Beneficios.Add("Transparencia con opinión independiente");
				DetalleItemValorizacion.Beneficios.Add("Equipo especialista");
				//DetalleItemValorizacion.Caracteristicas = new List<Models.TipoLista>();
				//DetalleItemValorizacion.Caracteristicas.Add(new Models.TipoLista { Encabezado = "El Servicio de Custodio incluyen", Contenidos = new List<string> { "Custodia Física y/o Anotación en cuenta de Valores", "Reportes mensuales de Estados de Cuenta y su valorización", "Depósito y Retiro de Títulos", "Cobro de dividendos, acciones liberadas y cupones.", "Registro Electrónico de Valores", "Registro de Compras y Ventas", "Transferencia de Valores", "Certificados de Custodia" } });
				DetalleItemValorizacion.Requisitos = new List<string>();
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemValorizacion);
				DetalleItemValorizacion.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú.");
				DetalleItemValorizacion.Requisitos.Add("Contrato de Servicios");


				Models.DetalleItemMenu DetalleItemNegociacioValoresCorp = new Models.DetalleItemMenu();
				DetalleItemNegociacioValoresCorp.Id = 126;
				DetalleItemNegociacioValoresCorp.IdItemMenu = Models.Constantes.ITEM_NEGOCIACION_CORP;
				DetalleItemNegociacioValoresCorp.Encabezado = "Negociación de Valores";
				DetalleItemNegociacioValoresCorp.Descripcion = "Somos especialistas en el asesoramiento personalizado para la ejecución de órdenes locales y globales en el mercado de capitales, nos enfocamos en una asesoría para un portafolio diversificado con niveles fundamentales y técnicos acorde al nivel de tolerancia al riesgo de cada cliente.";
				DetalleItemNegociacioValoresCorp.Beneficios = new List<string>();
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Ejecución Diligente");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Mesa de Trading a disposición del cliente durante el horario de negociación en Bolsa de Valores de Lima");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Comisiones competitivas a nivel del mercado");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Inmediata respuesta en ejecución de órdenes");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Ejecución de Órdenes Locales");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Acceso a las operaciones de renta fija y renta variable");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Todas las posiciones de los clientes quedan custodiadas en BNB Valores Perú a través de CAVALI");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Ejecución de Órdenes Extranjeras");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Acceso a las operaciones de renta fija y renta variable");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Acceso a principales mercados del mundo");
				DetalleItemNegociacioValoresCorp.Beneficios.Add("Liquidaciones en Cuenta Global, DVP");
				DetalleItemNegociacioValoresCorp.Caracteristicas = new List<Models.TipoLista>();
				DetalleItemNegociacioValoresCorp.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Renta Variable", Contenidos = new List<string> { "Acciones listadas en la Bolsa de Valores de Lima (BVL)", "Acciones Extranjeras listadas en la BVL y listadas en Bolsa Extranjera.", "Exchange Traded Funds, listados en la BVL " } });
				DetalleItemNegociacioValoresCorp.Caracteristicas.Add(new Models.TipoLista { Encabezado = "Renta Fija", Contenidos = new List<string> { "Papeles Comerciales", "Bonos de Arrendamiento Financiero.", "Bonos Corporativos.", "Bonos de Titulización.", "Bonos Subordinados.", "Bonos Soberanos.", "Bonos Estructurados.", "Operaciones de Reporte", "Letras del Tesoro Público.", "Facturas Negociables" } });
				DetalleItemNegociacioValoresCorp.Requisitos = new List<string>();
				DetalleItemNegociacioValoresCorp.Requisitos.Add("Apertura de cuenta de inversión en BNB Valores Perú");
				TipoRespuesta.DetalleItemsMenu.Add(DetalleItemNegociacioValoresCorp);

			}
			catch (Exception ex)
			{
				TipoRespuesta.OcurrioError = true;
				TipoRespuesta.Mensaje = ex.Message;
				TipoRespuesta.Descripcion = ex.StackTrace;
				TipoRespuesta.TipoError = Models.Enumeradores.TipoError.Error;
			}
			return TipoRespuesta;
		}
	}
}