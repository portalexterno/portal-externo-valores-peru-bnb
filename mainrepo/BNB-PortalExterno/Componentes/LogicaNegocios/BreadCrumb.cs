﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB_PortalExterno.Componentes.LogicaNegocios
{
  public class BreadCrumb
  {

    public static Metodos.BreadCrumb.ObtenerPorItem.TipoRespuesta ObtenerPorItem(Metodos.BreadCrumb.ObtenerPorItem.TipoEnvio TipoEnvio)
    {
      Metodos.BreadCrumb.ObtenerPorItem.TipoRespuesta TipoRespuesta = new Metodos.BreadCrumb.ObtenerPorItem.TipoRespuesta();
      try
      {
        TipoRespuesta.BreadCrumb = new List<Models.BreadCrumb>();
        //Asignacion del item.
        TipoRespuesta.BreadCrumb.Add(new Models.BreadCrumb { Accion = TipoEnvio.Item.Accion, Area = TipoEnvio.Item.Area, Controlador = TipoEnvio.Item.Controlador, Desripcion = TipoEnvio.Item.Nombre, TieneImagen = false, RutaImagen = string.Empty, Id = TipoEnvio.Item.Id});
        int IdItem = TipoEnvio.Item.IdSuperior;
        while (IdItem!= 0)
        {
          //Asignacion del items superiores.
          var Item = (HttpContext.Current.Application[Models.Constantes.MENU] as List<Models.Menu>).Find(x => x.Id == IdItem);
          TipoRespuesta.BreadCrumb.Add(new Models.BreadCrumb { Accion = Item.Accion, Area = Item.Area, Controlador = Item.Controlador, Desripcion = Item.Nombre, TieneImagen = false, RutaImagen = string.Empty, Id = Item.Id });
          IdItem = Item.IdSuperior;
        }
      }
      catch (Exception ex)
      {
        TipoRespuesta.OcurrioError = true;
        TipoRespuesta.Mensaje = ex.Message;
        TipoRespuesta.Descripcion = ex.StackTrace;
        TipoRespuesta.TipoError = Models.Enumeradores.TipoError.Error;
      }
      return TipoRespuesta;
    }
  }
}