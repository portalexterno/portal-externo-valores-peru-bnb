﻿var numeroValido;
var codigoValido;
var correoValido;

$(document).ready(function () {
  //fbq('track', 'ViewContent');
  codigoValido = false;
  correoValido = false;
  documentoValido = false;
});

$('li:has([data-toggle="tab"])').click(function (event) {
  return false;
});


function ValidarLimite(event, obj) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);
  if (obj.value == 1) {
    $("#PreAprobacion_LimiteTarjeta").prop('disabled', false);
    $("#PreAprobacion_LimiteTarjeta").prop('required', true);
    $("#PreAprobacion_LimiteTarjeta").focus();
  }
  else {
    $("#PreAprobacion_LimiteTarjeta").prop('disabled', true);
    $("#PreAprobacion_LimiteTarjeta").prop('required', false);
    $("#PreAprobacion_LimiteTarjeta").val("");
  }
}

function CalcularMonto(event) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);

  //var v = grecaptcha.getResponse();
  //if (v.length == 0) {
  //  swal("Tarjetas de crédito-BNB", "Por favor realice la verificación de seguridad.", "error");
  //  return;
  //}    

  var form = $("#Registro_Form");
  form.validate();
  if (form.valid()) {
    //Enviar la informacion al metodo de registro
    var Objeto = {
      a1: $("#Persona_Edad").val(),
      a2: $("#PreAprobacion_IngresoNeto").val(),
      a3: $("#PreAprobacion_TieneTarjetaCredito").val(),
      a4: $("#PreAprobacion_LimiteTarjeta").val(),
      a5: $("#PreAprobacion_CuotaConsumoOtrosBancos").val(),
      a6: $("#PreAprobacion_CuotaHipotecaVivienda").val(),
      a7: $("#PreAprobacion_AbonoSalarioBnb").val()
    }
    mostrarcargando("Estamos procesando su solicitud, por favor espere");
    $.ajax({
      async: true,
      cache: false,
      dataType: "json",
      data: Objeto,
      type: 'POST',
      url: '/TarjetasCredito/Calcular',
      success: function (respuesta) {
        ocultarcargando();
        if (respuesta.Correcto == true) {
          var contenidoHtml = "";
          contenidoHtml +=
            //'<b>'+ respuesta.Mensaje +'</b><br/>' +
            '<b><strong>Felicidades</strong></b>, Tienes pre aprobada una tarjeta de crédito en el BNB<br/>' +
            '<img class="img-responsive" style="display: block;margin-left: auto;margin-right: auto;" src="' + respuesta.TarjetaCredito.UrlImagen + '" /><br/>' +
            '<br/>Tipo de tarjeta:<b>' + respuesta.TarjetaCredito.Nombre + '</b><br/>' +
            '<span>Límite pre aprobado:' + 'Bs' + respuesta.TarjetaCredito.LimitePreAprobado.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span>'
          swal({
            title: "Tarjetas de crédito-BNB",
            text: respuesta.Mensaje,
            type: "success",
            html:contenidoHtml,
            showCancelButton: true,
            confirmButtonColor: '#5cb85c',
            confirmButtonText: 'Solicitar tarjeta ahora',
            cancelButtonText: 'Calcular otra vez',
            cancelButtonColor: '#b94a48',
            allowOutsideClick: false,
          }).then(function () {
            $('#myTab a[href="#dos"]').tab('show');
            $("html, body").animate({ scrollTop: 0 }, "slow");

            //Generar la informacion en la pantalla resumen
            $("#TarjetaCredito").html('Esta es la Tarjeta de crédito ideal para ti:' + respuesta.TarjetaCredito.Nombre);
            $("#ImagenTarjetaCredito").attr("src", respuesta.TarjetaCredito.UrlImagen);
            $("#ResumenTarjetaCredito").html('La aprobación de la presente solicitud esta sujeta a validación declara en el formulario. El banco se reserva el derecho de solicitar informacion adicional dependiendo de los respaldos presentados.');

          }, function (dismiss) {
            if (dismiss === 'cancel') {
              $("#Persona_Edad").focus();
            }
          });
        } else {
          swal({
            title: "Tarjetas de crédito-BNB",
            text: respuesta.Mensaje,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#6bc212',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false,
          }).then(function () {
            return false;
          });
        }
      },
      error: function (request, status, error) {
        ocultarcargando();
        var IndexUrl = '/TarjetasCredito/PreAprobacion';
        window.location.href = IndexUrl;
      }
    });
  }
}

function EventoTab(event, numeroTab) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);
  var form = $("#Registro_Form");
  form.validate();
  if (form.valid()) {
    $('#myTab a[href="#' + numeroTab + '"]').tab('show');
  }
  $("html, body").animate({ scrollTop: 0 }, "slow");
}

function ReturnTab(event, numeroTab) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);
  $('#myTab a[href="#' + numeroTab + '"]').tab('show');
  $("html, body").animate({ scrollTop: 0 }, "slow");
}
function ValidarResidencia(event, obj) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);
  if (obj.checked) {
    obj.value = true;
    if (document.getElementById("Persona_DocumentoResidenciaEEUU") === null) {
      $("#ciudadano").append("<label class='control-label' for='Persona_DocumentoResidenciaEEUU'>No. de identificación tributaria o seguro social</label><input class='form-control input-sm' data-val='true' data-val-required='Información requerida.' id='Persona_DocumentoResidenciaEEUU' name='Persona.DocumentoResidenciaEEUU' type='text' maxlength='25'  value=''><span class='field-validation-valid text-danger' data-valmsg-for='Persona.DocumentoResidenciaEEUU' data-valmsg-replace='true'></span>");
      $("input[id*=Persona_DocumentoResidenciaEEUU]").rules("add", { required: true, messages: { required: "Información requerida" } });
    }
  } else {
    $("#ciudadano").empty();
    obj.value = false;
  }
}
function ValidarIncapacidad(e, obj) {
  if (e != null) e.preventDefault();
  if (obj.value != 3) {
    $("#incapacidad").empty();
  }
  else {
    if (document.getElementById("Persona_Incapacidad") === null) {
      $("#incapacidad").append("<label class='control-label' for='Persona_Incapacidad'>Ingrese la discapacidad</label><input class='form-control input-sm' data-val='true' data-val-required='Información requerida.' id='Persona_Incapacidad' name='Persona.Incapacidad'  maxlength='50' type='text' value=''><span class='field-validation-valid text-danger' data-valmsg-for='Persona.Incapacidad' data-valmsg-replace='true'></span>");
      $("input[id*=Persona_Incapacidad]").rules("add", { required: true, messages: { required: "Información requerida" } });
    }
  }
}

//Codigo de confirmacion
function solicitarCodigoConfirmacion(e) {
  if (e != null) e.preventDefault();
  $('#Persona_Celular').valid();
  if ($('#Persona_Celular').val() != '' && $('#Persona_Celular').valid()) {
    //Envio la informacion del cliente para obtener el codigo de verificacion.
    var Objeto = { NumeroDocumento: $("#Persona_NumeroDocumento").val(), Complemento: $("#Persona_Complemento").val(), NumeroCelular: $('#Persona_Celular').val() };
    mostrarcargando("Solicitando código de confirmación, por favor espere");
    $.ajax({
      async: true,
      cache: false,
      dataType: "html",
      data: Objeto,
      type: 'POST',
      url: '/TarjetasCredito/EnviarSms',
      success: function (respuesta) {
        ocultarcargando();
        respuesta = JSON.parse(respuesta);
        if (respuesta.correcto == true) {
          numeroValido = true;
          if ($("#scs") != null) {
            $("#scs").remove();
          }
          $("<span id='scs' class='input-group-addon colorVerde'><i class='glyphicon glyphicon-ok'></i></span>").appendTo("#grupoPersonaCelular");
        }
        else {
          if ($("#scs") != null) {
            $("#scs").remove();
          }
          swal("Tarjetas de crédito-BNB", "Error: " + respuesta.mensaje, "error");
          //cambiado
          numeroValido = true;
        }
      },
      error: function (request, status, error) {
        ocultarcargando();
        swal("Tarjetas de crédito-BNB", "Error: " + request.responseText, "error");
        numeroValido = false;
      }
    });
  }
}
function VerificarCodigoConfirmacion(e) {
  if (e != null) e.preventDefault();
  $('#codigoconfirmacion').valid();
  if ($('#codigoconfirmacion').valid()) {
    var Objeto = { NumeroDocumento: $("#Persona_NumeroDocumento").val(), Complemento: $("#Persona_Complemento").val(), NumeroCelular: $('#Persona_Celular').val(), Codigo: $('#codigoconfirmacion').val() };
    mostrarcargando("Validando el código de confirmación, por favor espere");
    $.ajax({
      async: true,
      cache: false,
      dataType: "html",
      data: Objeto,
      type: 'POST',
      url: '/TarjetasCredito/ValidarSms',
      success: function (respuesta) {
        ocultarcargando();
        respuesta = JSON.parse(respuesta);
        if (respuesta.correcto == true) {
          codigoValido = true; if ($("#vcc") != null) { $("#vcc").remove(); } $("<span id='vcc' class='input-group-addon colorVerde'><i class='glyphicon glyphicon-ok'></i></span>").appendTo("#grupoCodigoConfirmacion");
          numeroValido = true; if ($("#scs") != null) { $("#scs").remove(); } $("<span id='scs' class='input-group-addon colorVerde'><i class='glyphicon glyphicon-ok'></i></span>").appendTo("#grupoPersonaCelular");
        }
        else {
          swal("Tarjetas de crédito-BNB", respuesta.mensaje, "error");
          if ($("#vcc") != null) {
            $("#vcc").remove();
          }
          codigoValido = false;
        }
      },
      error: function (request, status, error) {
        ocultarcargando();
        swal("Tarjetas de crédito-BNB", request.responseText, "error");
        codigoValido = false;
      }
    });
  }
}
function VerificarEmail(e) {
  if (e != null) e.preventDefault();
  $('#Persona_CorreoElectronico').valid();
  if ($('#Persona_CorreoElectronico').valid()) {
    if (!correoValido) {
      var Objeto = { CorreoElectronico: $("#Persona_CorreoElectronico").val() };
      mostrarcargando("Verificando el correo electrónico, por favor espere");
      $.ajax({
        async: true,
        cache: false,
        dataType: "html",
        data: Objeto,
        type: 'POST',
        url: '/TarjetasCredito/ValidarCorreoElectronico/',
        success: function (respuesta) {
          ocultarcargando();
          respuesta = JSON.parse(respuesta);
          if (respuesta.correcto == true) {
            correoValido = true;
            if ($("#vce") != null) {
              $("#vce").remove();
            }
            $("<span id='vce' class='input-group-addon colorVerde'><i class='glyphicon glyphicon-ok'></i></span>").appendTo("#grupoPersonaCorreoElectronico");
          }
          else {
            if ($("#vce") != null) {
              $("#vce").remove();
            }
            swal("Tarjetas de crédito-BNB", respuesta.mensaje, "error");
            correoValido = false;
          }
        },
        error: function (request, status, error) {
          ocultarcargando();
          swal("Tarjetas de crédito-BNB", request.responseText, "error");
          correoValido = false;
        }
      });
    }
  }
}
function EvntGuardar(event) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);

  if (!numeroValido) {
    swal("Tarjetas de crédito-BNB", "Por favor valide su número de celular", "error");
    return false;
  }
  if (!correoValido) {
    swal("Tarjetas de crédito-BNB", "Por favor valide su correo electrónico", "error");
    return false;
  }
  if (!codigoValido) {
    swal("Tarjetas de crédito-BNB", "Por favor valide el código de confirmación", "error");
    return false;
  }

  var form = $("#Registro_Form");
  form.validate();
  if (!form.valid()) {
    return false;
  }

  var Objeto = {
    a1: $("#Persona_Nombres").val(),
    a2: $("#Persona_ApellidoPaterno").val(),
    a3: $("#Persona_ApellidoMaterno").val(),
    a4: $("#Persona_ApellidoCasada").val(),
    a5: $("#Persona_NumeroDocumento").val(),
    a6: $("#Persona_Complemento").val(),
    a7: $("#Persona_CodigoExtencionDocumento").val(),
    a8: $("#Persona_CodigoGenero").val(),
    a9: $("#Persona_CodigoEstadoCivil").val(),
    a10: $("#Persona_FechaNacimiento").val(),
    a11: $("#Direccion_Domicilio").val(),

    a12: $("#ActividadEconomica_CodigoProfesion").val(),
    a14: $("#ActividadEconomica_CodigoTipoLaboral").val(),
    a13: $("#ActividadEconomica_CodigoSituacionLaboral").val(),
    a16: $("#ActividadEconomica_CodigoActividadEconomicaLaboral").val(),
      
    a18: $("#Persona_EsCiudadanoEEUU").val(),
    a19: $("#Persona_DocumentoResidenciaEEUU").val(),
    a20: $("#Persona_TipoIncapacidad").val(),
    a21: $("#Persona_Incapacidad").val(),

    a22: $("#Persona_Telefono").val(),
    a23: $("#Persona_Celular").val(),
    a24: $("#Persona_CorreoElectronico").val(),
  };
  mostrarcargando("Procesando su solicitud, por favor espere");
  $.ajax({
    async: true,
    cache: false,
    dataType: "html",
    data: Objeto,
    type: 'POST',
    url: '/TarjetasCredito/PreAprobacion',
    success: function (respuesta) {
      ocultarcargando();
      respuesta = JSON.parse(respuesta);
      if (respuesta.correcto == true) {
        //fbq('track', 'Lead');
        swal({
          title: "Tarjetas de crédito-BNB",
          text: respuesta.mensaje,
          type: "success",
          showCancelButton: false,
          confirmButtonColor: '#42b72a',
          confirmButtonText: 'Aceptar',
        }).then(function () {
          var IndexUrl = '/TarjetasCredito/PreAprobacion';
          window.location.href = IndexUrl;
        });
      }
      else {
        swal({
          title: "Tarjetas de crédito-BNB",
          text: respuesta.mensaje,
          type: "error",
          showCancelButton: false,
          confirmButtonColor: '#42b72a',
          confirmButtonText: 'Ok',
        });
      }
    },
    error: function (request, status, error) {
      alert(error);
      ocultarcargando();
      var IndexUrl = '/TarjetasCredito/PreAprobacion")';
      window.location.href = IndexUrl;
      }
  });
}

$("#Persona_CorreoElectronico").change(function () {
  correoValido = false;
  if ($("#vce") != null) {
    $("#vce").remove();
  }
});
$("#Persona_CorreoElectronico").focusout(function () {
  VerificarEmail(this.event);
});
$("#Persona_Celular").change(function () {
  numeroValido = false;
  codigoValido = false;
  if ($("#scs") != null) {
    $("#scs").remove();
  }
  if ($("#vcc") != null) {
    $("#vcc").remove();
  }
});