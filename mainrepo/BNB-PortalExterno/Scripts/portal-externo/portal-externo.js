﻿//Mostrar pantall de cargando
function mostrarcargando(mensaje) {
  var e = document.getElementById('procesando');
  e.querySelector('#mensaje').innerHTML = mensaje;
  e.querySelector('#divLoading').style.display = 'block';
};
//Ocultar pantalla de cargando
function ocultarcargando() {
  var e = document.getElementById('procesando');
  e.querySelector('#divLoading').style.display = 'none';
};
//Funcion para habilitar el encabezado flotante
//$(function () {
//  var menu = $('.navbar.navbar-default.navbar-bnb'),
//    pos = menu.offset();
//  $(window).scroll(function () {
//    if ($(window).width() > 767) {
//      if ($(this).scrollTop() > pos.top + menu.height() && menu.hasClass('navbar')) {
//        menu.fadeOut('fast', function () {
//          $(this).removeClass('navbar').addClass('navbar-fixed').fadeIn('fast');
//        });
//      } else if ($(this).scrollTop() <= pos.top + 121 && menu.hasClass('navbar-fixed')) {
//        menu.fadeOut('fast', function () {
//          $(this).removeClass('navbar-fixed').addClass('navbar').fadeIn('fast');
//        });
//      }
//    }
//  });
//});
//Cambiar las imagenes que contengan la palabra varde y blanco
function CambiarColorIcono(event, obj) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);
  var srcActual = $(obj).find('img').attr("src");
  srcActual = srcActual.toLowerCase().replace("blanco", "verde");
  $(obj).find('img').attr("src", srcActual)
}

function RestaurarColorIcono(event, obj) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);
  var srcActual = $(obj).find('img').attr("src");
  srcActual = srcActual.toLowerCase().replace("verde", "blanco");
  $(obj).find('img').attr("src", srcActual)
}

//funcion para habilitar el seguimiento de carlitos.
$(window).scroll(function () {
    var winScrollTop = $(window).scrollTop();
    var winHeight = $(window).height();
    var floaterHeight = $('#floater').outerHeight(true);
    var fromBottom = 5;
    var top = winScrollTop + winHeight - floaterHeight - fromBottom;
    $('#floater').css({ 'top': top + 'px' });
});