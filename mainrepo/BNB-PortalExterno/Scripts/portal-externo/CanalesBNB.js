﻿

$(document).ready(function () {
  // Asigna clase a ciudad elegida
  $(".menu-ciudad a").click(function () {

    $(".menu-ciudad a").each(function (index) {
      $(this).removeClass("Elegido");
    });

    $(this).addClass("Elegido");

    // Obtiene texto de opcion y asigna a titulo
    $("#titPagina").html($(this).children().html());

    $("#txtBuscador").val("");
  });


});

// Funcionalidad de canales
TipoMover = {
  Adelante: 1,
  Atras: 2
};

TipoCanal = {
  SucursalesAgencias: 1,
  ATM: 2,
  BNBJoven_Comida: 3,
  BNBJoven_Entretenimiento: 4,
  BNBJoven_Tiendas: 5,
  BNBJoven_Otros: 6,
  Virtuales: 7,
  Obtener_Titulo: function (enuTipo) {
    var strTitulo = '';

    switch (enuTipo) {
      case TipoCanal.SucursalesAgencias:
        strTitulo = 'Sucursales Agencias';
        break;
      case TipoCanal.ATM:
        strTitulo = 'Cajeros Automáticos';
        break;
      case TipoCanal.Virtuales:
        strTitulo = 'Terminales de AutoServicio';
        break;
      default:
        strTitulo = 'Indefinido';
        break;

    };

    return strTitulo;
  }
};

Tipo = {
  Indefinido: 0,
  Autobanco: 1,
  OficinaPrincipal: 2,
  AgenciaFija: 3,
  BNBExpress: 4,
  CajaExterna: 5,
  PuntoPromocional: 6,
  Ventanilla: 7,
  Obtener_Titulo: function (enuTipo) {
    var strTitulo = '';

    switch (enuTipo) {
      case Tipo.Autobanco:
        strTitulo = 'Auto Banco';
        break;
      case Tipo.OficinaPrincipal:
        strTitulo = 'Oficina Principal';
        break;
      case Tipo.AgenciaFija:
        strTitulo = 'Agencia Fija';
        break;
      case Tipo.BNBExpress:
        strTitulo = 'BNB Express';
        break;
      case Tipo.CajaExterna:
        strTitulo = 'Caja Externa';
        break;
      case Tipo.PuntoPromocional:
        strTitulo = 'Punto Promocional';
        break;
      case Tipo.Ventanilla:
        strTitulo = 'Ventanillas';
        break;
      default:
        strTitulo = 'Indefinido';
        break;

    };

    return strTitulo;
  }
}

//Variable que indica la URL donde estan las imágenes
var URLImagenes = '../imagenes/administrador/';

var _objGeolocalizacionBNB = {
  Configuracion: {
    Bolivia: {
      Longitud: -68.134388,
      Latitud: -16.498859
    },
    IDPanelMapa: "MapaGoogle",
    IDPanelCargando: "#DetalleCanalesBNB #MensajeCargandoMapa",
    IDPanelEntidades: "#ListaEntidades",
    IDPanelEntidadesItems: "#ListaEntidades div.Items",
    IDPanelDetalleEntidad: "#DetalleCanalesBNB #DetalleEntidad",
    IDSpanlItemSeleccionado: "#ListaEntidades #spanddlItemSeleccionado",
    IDcmbFiltros: "#ddlFiltroGrupos",

    //Desde 22-Junio-2016, Google requiere una llave (key) para el uso de su API de Google Maps. Por esta razón se obtuvo una llave
    //con el usuario del banco (bnbmovil2013@gmail.com) para el proyecto "Google Maps para Portal BNB".
    //La llave asignada es: AIzaSyBScRShsqpcppZzVzlxcWDSaLNfs_K-FIE
    //Fuente: http://googlegeodevelopers.blogspot.com.es/2016/06/building-for-scale-updates-to-google.html
    //Esta llave fue generada y agregada a la URLApi de Google Maps el 15.Agosto.2016
    //URLApi: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBScRShsqpcppZzVzlxcWDSaLNfs_K-FIE&v=3.exp&callback=_objGeolocalizacionBNB.CargarMapa',
    URLApi: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDwf4kR2OCCofCwEhvdsAzO3i-hVyUUzSI&callback=_objGeolocalizacionBNB.CargarMapa',

    Filtro_Ciudades: "#ContenidoCanalesBNB #ListaCiudades .Ciudad a",
    Filtro_Entidades: "#DetalleCanalesBNB #ListaEntidades .Items .Contenido a",
    Class_Seleccionada: "Elegido",
    Entidades_Paginacion: {
      CantidadXPagina: 1000,//10 cantidad de item por pagina
      IDAtras: "#ListaEntidades .Paginacion #Atras",
      IDSiguiente: "#ListaEntidades .Paginacion #Siguiente",
      IDNroPagina: "#ListaEntidades .Paginacion #NroPagina",
      IDSeparadorPagina: "#ListaEntidades .Paginacion #SeparadorPaginacion"
    },
    Iconos: {
      Verde: {
        Nombre: 'Verde',
        // Ruta: URLImagenes + 'Verde_50.png'
        Ruta: '../Images/Iconos/Verde_50.png'

      },
      Verde_small: {
        Nombre: 'VerdeSmall',
        //Ruta: URLImagenes + 'Verde_25.png'
        Ruta: '../Images/Iconos/Verde_25.png'
      }
    }

  },

  Parametros: {
    Latitud: null,
    Longitud: null,
    IDCiudadSeleccionada: -1,
    TabSeleccionado: null,
    Mapa: null
  },

  ParametrosRecibidos: {
    Ciudad: -1,
    Elemento: -1,
    Tipo: -1,
    Subtipo: -1
  },

  //Funcion inicial que carga datos y configura la pantalla
  Cargar: function () {

    //Cargar parametros recibidos por Query String (si es que existen)
    this.CargarParametros();

    //Obtener la informacion necesaria dependiendo si se recibieron parametros por Querystring o se utilizaran los por defecto
    if (this.ParametrosRecibidos.Ciudad > 0) {

      //Se obtiene la Coordenada Inicial según la Ciudad y Tipo recibido por Querystring
      this.Cargar_CiudadYTab_Recibidos();

    } else {

      //Se obtiene la Coordenada Inicial según la Ciudad y Tipo seleccionado por defecto
      this.Cargar_CiudadYTab();
    }

    //Se Cargar el Script del Google
    this.CargarScriptGoogle();

  },

  //Si la pagina fue llamada pasandole parametros por querystring (esto sucede cuando se lo llama desde la pagina
  //busqueda), esta funcion obtiene todos los parametros recibidos
  CargarParametros: function () {
    var codigos = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

    if (codigos.length == 4) {
      this.ParametrosRecibidos.Ciudad = codigos[0];
      this.ParametrosRecibidos.Elemento = codigos[1];
      this.ParametrosRecibidos.Tipo = codigos[2];
      this.ParametrosRecibidos.Subtipo = codigos[3];
    }
  },

  //Obtiene las coordenadas iniciales de Ciudad y Tipo (agencia, cajero, etc.)
  Cargar_CiudadYTab: function () {
    this.Cargar_Ciudad();
    this.Cargar_Tab();
  },
  Cargar_CiudadYTab_Recibidos: function () {
    this.Cargar_Ciudad_Recibida();
    this.Cargar_Tab_Recibido();
  },

  //Obtener la ciudad actualmente seleccionada
  Cargar_Ciudad: function () {
    var intIDCiudad = this.Obtener_IDCiudadSeleccionada();
    this.Parametros.IDCiudadSeleccionada = intIDCiudad;
  },
  Cargar_Ciudad_Recibida: function () {
    this.Parametros.IDCiudadSeleccionada = this.ParametrosRecibidos.Ciudad;
  },

  //Obtiene los datos del Tipo (grupo) actualmente seleccionado
  Cargar_Tab: function () {
    var objTab = this.Obtener_TabSeleccionado();
    this.Parametros.TabSeleccionado = objTab;
    this.Cargar_CoordenadaCenter();
  },
  Cargar_Tab_Recibido: function () {
    var objTab = this.Obtener_TabSeleccionado_Recibido();
    this.Parametros.TabSeleccionado = objTab;
    this.Cargar_CoordenadaCenter();
  },

  //Obtiene la ciudad que fué seleccionada en la pantalla
  Obtener_IDCiudadSeleccionada: function () {
    var Query = this.Configuracion.Filtro_Ciudades + '.' + this.Configuracion.Class_Seleccionada;
    var href = $(Query).attr('href').toString();
    return parseInt(href.split("_")[1]);
  },

  //Obtiene el Tipo (oficina, agencia, cajero, autobanco, etc.) seleccionado en la pantalla
  Obtener_TabSeleccionado: function () {
    var arrTipos = [];
    //Ahora se utiliza el Cmb de filtro por grupos
    //var QUERY = this.Configuracion.IDcmbFiltros + ' :selected';
    var Value = $(this.Configuracion.IDcmbFiltros).val().toString();
    var arrSplit = Value.split("_");
    //El primer item es el Tipo Canal
    var enuCanal = parseInt(arrSplit[0]);
    //después son los Tipos que se agruparían por item del cmb (inicialmente solamente se tiene un tipo, antes se podía tener varios, por eso el ciclo FOR)
    if (arrSplit.length > 1) {
      for (i = 1; i < arrSplit.length; i++) {
        arrTipos.push(parseInt(arrSplit[i]));
      };
    };

    var objTab = { Canal: enuCanal, Ver: arrTipos }
    return objTab;
  },
  Obtener_TabSeleccionado_Recibido: function () {
    //Obtener Tipo Canal y Subtipo
    var arrTipos = [];
    var enuCanal = this.ParametrosRecibidos.Tipo;
    arrTipos.push(parseInt(this.ParametrosRecibidos.Subtipo));
    //Armar objeto Tab
    var objTab = { Canal: enuCanal, Ver: arrTipos };
    return objTab;
  },

  //Obtiene las coordenadas (latitud y longitud) del elemento seleccionado
  Cargar_CoordenadaCenter: function () {
    var objCiudad = this.Obtener_CiudadSeleccionada();
    var arrDetalles = [];
    if (objCiudad != null) {
      arrDetalles = this.Obtener_DetallesParaVer(objCiudad);
      if (arrDetalles != null && arrDetalles.length > 0) {
        this.Parametros.Latitud = arrDetalles[0].Latitud;
        this.Parametros.Longitud = arrDetalles[0].Longitud;
      };
    };
    if (this.Parametros.Longitud == null || isNaN(this.Parametros.Longitud)) {
      this.Parametros.Longitud = this.Configuracion.Bolivia.Longitud;
      this.Parametros.Latitud = this.Configuracion.Bolivia.Latitud;
    };
  },

  //Obtener Toda la informacion de la Ciudad actualmente seleccionada
  //OJO: La variable 'Datos' está definida en el archivo 'Datos.js' al cual hace referencia la pagina de Canales BNB
  Obtener_CiudadSeleccionada: function () {
    if (this.Parametros.IDCiudadSeleccionada > 0) {
      var intLongitudCiudades = Datos.length;
      if (intLongitudCiudades > 0) {
        for (i = 0; i < intLongitudCiudades; i++) {
          if (Datos[i].Ciudad.ID == this.Parametros.IDCiudadSeleccionada) {
            return Datos[i].Ciudad;
          };
        };
      };
    };
    return null;
  },

  //Obtiene toda la informacion del Tipo (cajeros, agencias, etc.) actualmente seleccionado
  Obtener_DetallesParaVer: function (objCiudad) {
    var arrDetalles = [];
    if (objCiudad != null && this.Parametros.TabSeleccionado != null) {
      var intLogitudDetalles = objCiudad.Detalles.length;
      var objTab = this.Parametros.TabSeleccionado;
      if (intLogitudDetalles > 0) {
        for (j = 0; j <= intLogitudDetalles - 1; j++) {
          if (objCiudad.Detalles[j].Tipo == objTab.Canal && objTab.Ver.indexOf((objCiudad.Detalles[j].SubTipo)) >= 0) {
            arrDetalles.push(objCiudad.Detalles[j]);
          };
        };
      };

    };
    return arrDetalles;
  },

  //Agrega el script para Google Maps en el html de Canales BNB
  CargarScriptGoogle: function () {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = this.Configuracion.URLApi;
    document.body.appendChild(script);
  },

  //Función llamada por Google Maps API una vez que terminó de cargar la API asíncronamente
  CargarMapa: function () {

    //Extensión
    (function () {
      google.maps.Map.prototype.Marcadores = new Array();
      google.maps.Map.prototype.LimpiarMarcadores = function () {
        for (var i = 0; i < this.Marcadores.length; i++) {
          this.Marcadores[i].setMap(null);
        };

        this.Marcadores = new Array();
      };
      google.maps.Marker.prototype.Detalle = null;
      google.maps.Marker.prototype.InfoResumen = null;
      google.maps.Map.prototype.CerrrarTodosInfoWin = function () {
        for (var i = 0; i < this.Marcadores.length; i++) {
          if (this.Marcadores[i].InfoResumen != null) this.Marcadores[i].InfoResumen.close();
        };

      };

      //Función para que cuando se asigne al mapa también se agregue el Arreglo de Marcadores
      //se debe Guardar en una variable Aux, la antigua clase o la clase Base si vale el termino para poder seguir utilizando la función del google map de agregar el marcador.
      var AntiguoSetMap = google.maps.Marker.prototype.setMap;
      google.maps.Marker.prototype.setMap = function (map) {
        if (map) {
          map.Marcadores.push(this);
        }
        AntiguoSetMap.call(this, map);
      }

    })();

    var mapOptions = {
      zoom: 18,
      center: new google.maps.LatLng(this.Parametros.Latitud, this.Parametros.Longitud),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById(this.Configuracion.IDPanelMapa), mapOptions);
    this.Parametros.Mapa = map;

    this.Colocar_Marcadores();

    $(this.Configuracion.IDPanelCargando).hide();

    //Ciudades
    $(this.Configuracion.Filtro_Ciudades).click(function (e) {
      e.preventDefault();

      //Asigna opcion a combo ciudad de movil
      var strCiudadSel = $(this).children("span").html();
      $("#cmbCiudad").find("option:selected").text(strCiudadSel);

      //Ocultar el Detalle de la Entidad
      _objGeolocalizacionBNB.Ocultar_Detalle();

      //Quitar los Marcadores
      _objGeolocalizacionBNB.Parametros.Mapa.LimpiarMarcadores();

      //Quitar el Estilo Seleccionado a todos los Link Izquierda
      _objGeolocalizacionBNB.Quitar_Seleccionada(_objGeolocalizacionBNB.Configuracion.Filtro_Ciudades);

      //Agregar el estilo de ciudad Seleccionada al Menu de Ciudades (links de la izquierda)
      $(this).addClass(_objGeolocalizacionBNB.Configuracion.Class_Seleccionada);

      /*Cargar la Ciudad Seleccionada
      _objGeolocalizacionBNB.Cargar_Ciudad();*/

      //Restablecer al Primer Item seleccionado del combo de Grupos (esto provocaría un evento Change.....
      //por tanto este evento Cargaría la Ciudad y TabSeleccionado en el Mapa
      //el selectedIndex = 0; no produce el evento change del Combo en IE.
      //Inicialmente se quiere tener seleccionado el item "Oficina Principal" (cuyo indice es 5)
      //se coloca el Index = 5 (Oficina Principal) xq al generar los items del combo, se reordena alfabéticamente y no mantiene
      //el orden del enumerador
      $(_objGeolocalizacionBNB.Configuracion.IDcmbFiltros).get(0).selectedIndex = 5;  //5=Indice de "Oficina Principal" del combo
      _objGeolocalizacionBNB.Colocar_TituloFiltroSeleccionado();

      //Por tanto se carga la Ciudad y El Tab seleccionado y se colocan los marcadores
      _objGeolocalizacionBNB.Cargar_CiudadYTab();

      //Cargar los Marcadores
      _objGeolocalizacionBNB.Colocar_Marcadores();

    });

    // Cambio de indice combo ciudades, para moviles
    $("#cmbCiudad").change(function (e) {
      e.preventDefault();

      var valCiudad = $(this).val();

      $("#ddlFiltroGrupos")[0].selectedIndex = 5; // Oficina principal

      $(".menu-ciudad a").each(function (index) {
        var valCiudadEach = $(this).attr("href");

        $(this).removeClass("Elegido");
        if (valCiudad == valCiudadEach) {
          $(this).addClass("Elegido");
          // Obtiene texto de opcion y asigna a titulo
          $("#titPagina").html($(this).children().html());
        }

      });

      $(".menu-ciudad a").each(function (index) {
        var clase = $(this).attr("class");
        if (clase == "Elegido") {
          $(this).trigger("click");
        }
      });

    });

    //Tabs - Grupo de Filtros Cmb
    $(this.Configuracion.IDcmbFiltros).change(function () {

      var strValue = $(this).val();
      var strTexto = $(this).text();

      _objGeolocalizacionBNB.Colocar_TituloFiltroSeleccionado();

      //Ocultar el Detalle de la Entidad
      _objGeolocalizacionBNB.Ocultar_Detalle();

      //Quitar los Marcadores
      _objGeolocalizacionBNB.Parametros.Mapa.LimpiarMarcadores();

      //Cargar el Mapa con lo seleccionado
      _objGeolocalizacionBNB.Cargar_Tab();

      //Cargar los Marcadores
      _objGeolocalizacionBNB.Colocar_Marcadores();
    });

    //Siguiente y Atras
    $(this.Configuracion.Entidades_Paginacion.IDAtras).click(function (e) {
      e.preventDefault();
      _objGeolocalizacionBNB.Paginar(TipoMover.Atras);
    });
    $(this.Configuracion.Entidades_Paginacion.IDSiguiente).click(function (e) {
      e.preventDefault();
      _objGeolocalizacionBNB.Paginar(TipoMover.Adelante);
    });


    //Si la página fue llamada desde la pagina de busqueda, quiere decir que los parámetros del elemento seleccionado vienen
    //por Querystring y ya fueron cargados al iniciar la pagina (al ejecutar '_objGeolocalizacionBNB.Cargar()'). Estos parametros son
    //cargados en 'ParametrosRecibidos' entonces debe hacerse lo mismo que hace la pagina al seleccionar una ciudad (menu izquierda),
    //luego seleccionar un Grupo del combo y finalmente hacer click sobre uno de los elementos desplegados.
    //Si la página NO FUE llamada por la pagina de busqueda (osea se la llamó desde un link de la página principal del portal), no existen
    //parámetros por querystring y 'ParametrosRecibidos' no tiene datos (valores=-1) (porque se toman en cuenta los valores por defecto), por lo tanto,
    //no debe entrar a la siguiente condicion y debe seguir su ejecucion normal.
    if (this.ParametrosRecibidos.Ciudad > 0) {
      //Seleccionar en el menu de Ciudad, la ciudad seleccionada en el querystring
      _objGeolocalizacionBNB.Seleccionar_Menu_Ciudad();
      //Seleccionar en el Combo de Grupos (Agencias, cajeros, etc.) el grupo seleccionado en el querystring
      _objGeolocalizacionBNB.Seleccionar_Combo_Grupos();
      //Seleccionar en la lista de elementos desplegados (lista de Agencias, lista de cajeros, etc.) el elemento seleccionado en el querystring
      _objGeolocalizacionBNB.Seleccionar_Elemento();
    }

  },

  //Configura el Mapa para que se muestre en el elemento seleccionado (marcador y detalle), ademas, despliega toda la lista de elementos (segun el tipo o grupo
  //elegido en el combo) paginada
  Colocar_Marcadores: function () {
    var arrDetalles = [];
    var objCiudad = this.Obtener_CiudadSeleccionada();

    if (objCiudad != null) {
      arrDetalles = this.Obtener_DetallesParaVer(objCiudad);

      if (arrDetalles != null && arrDetalles.length > 0) {
        var intLongitudDetalles = arrDetalles.length
        for (i = 0; i <= intLongitudDetalles - 1; i++) {
          //Si el Marcador es el primero o único, es decir i==0, entonces se debe mostrar la marca con el InfoResumen Desplegado
          this.Crear_Marcador(arrDetalles[i], i == 0);
        };
        this.IrA(arrDetalles[0].Latitud, arrDetalles[0].Longitud);
      };

      //Configurar la Lista de Entidades, según una paginación
      //se toma como nro página la 1, la inicial
      this.Configurar_Paginacion(1, arrDetalles);
    };

  },

  //Genera y Despliega la marca sobre el elemento seleccionado y su correspondiente detalle bajo el mapa
  Crear_Marcador: function (objDetalle, boolMostrarInfoResumen) {
    if (objDetalle != null && this.Parametros.Mapa != null) {
      var iconoURL = this.Obtener_RutaImagenPorTipo(objDetalle.SubTipo, false);
      var GoogleLatlng = new google.maps.LatLng(objDetalle.Latitud, objDetalle.Longitud);
      var objMarcador = new google.maps.Marker({
        position: GoogleLatlng,
        map: this.Parametros.Mapa,
        icon: iconoURL,
        Detalle: objDetalle
      });

      //Crear el InfoWindows [resumen]
      //Ya no existe el link VerMas, ya se muestra todo al hacer click sobre el marcador
      var InfoResumen = new google.maps.InfoWindow({
        content: '<span class="Titulo">' + objDetalle.Descripcion + '</span>'
      });

      //Se adicionar el InfoResumen al Marcador para poder usarlo después (cerrar todos....)
      objMarcador.InfoResumen = InfoResumen

      if (boolMostrarInfoResumen == true) {
        this.Mostrar_InfoResumen(objMarcador);
      }

      //mostrar InfoResumen
      google.maps.event.addListener(objMarcador, 'click', function () {
        _objGeolocalizacionBNB.Mostrar_InfoResumen(objMarcador);
      });

    };
  },

  //Obtiene la ruta de las imagenes que se muestran como marcadores en el Mapa
  Obtener_RutaImagenPorTipo: function (enuTipo, boolSmall) {
    var strURLImg = '';
    //Ahora solamente se manejera un solo ícono
    if (boolSmall == true)
      strURLImg = _objGeolocalizacionBNB.Configuracion.Iconos.Verde_small.Ruta
    else
      strURLImg = _objGeolocalizacionBNB.Configuracion.Iconos.Verde.Ruta

    return strURLImg;
  },

  //Muestra el Marcador sobre el elemento seleccionado y muestra su detalle debajo del mapa
  Mostrar_InfoResumen: function (objMarcador) {
    objMarcador.InfoResumen.open(this.Parametros.Mapa, objMarcador);
    //ya muestra el detalle de la entidad seleccionada al hacer click
    _objGeolocalizacionBNB.Mostrar_Detalle(objMarcador.Detalle);
  },

  //Genera el HTML que se muestra en el area de detalle del elemento seleccionado
  Mostrar_Detalle: function (objDetalle) {
    var PanelDetalle = $(this.Configuracion.IDPanelDetalleEntidad + " .Detalle").first();

    //Se borra cualquier otro detalle existente
    $(PanelDetalle).html('');

    //Armar el Detalle en HTML
    var strHTML = '<span class="Titulo textoverde3c d16 fuente-bold ">' + objDetalle.Descripcion + '</span>' +
                  '<p class="SubTitulo textoplomo3d d16 ">' + objDetalle.Direccion + '</p>'

    //Teléfonos
    if ((objDetalle.Telefono1 != null && objDetalle.Telefono1.length > 0)
        || (objDetalle.Telefono2 != null && objDetalle.Telefono2.length > 0)
        || (objDetalle.Fax != null && objDetalle.Fax.length > 0)) {
      strHTML += '<p class=" textoplomo3d d16 " >Teléfonos ';
      //Telf 1
      if (objDetalle.Telefono1 != null && objDetalle.Telefono1.length > 0) {
        strHTML += '<span class=" textoplomo3d d16 " >' + objDetalle.Telefono1 + '</span>&nbsp;';
        //Interno 1
        if (objDetalle.Interno1 != null && objDetalle.Interno1.length > 0) {
          strHTML += '<span class=" textoplomo3d d16 " >(int ' + objDetalle.Interno1 + ') </span>&nbsp;';
        };
      };

      //Telf 2
      if (objDetalle.Telefono2 != null && objDetalle.Telefono2.length > 0) {
        strHTML += '<span>' + objDetalle.Telefono2 + '</span>&nbsp;';
        //Interno 2
        if (objDetalle.Interno2 != null && objDetalle.Interno2.length > 0) {
          strHTML += '<span>(int ' + objDetalle.Interno2 + ') </span>&nbsp;';
        };
      };

      //Fax
      if (objDetalle.Fax != null && objDetalle.Fax.length > 0) {
        strHTML += '<span>' + objDetalle.Fax + ' (fax)</span>';
      };
      strHTML += '</p>'

    };


    //Agregar Horarios, solamente si tiene
    if (objDetalle.Horarios != null && objDetalle.Horarios.length > 0) {
      strHTML += '<p>Horarios de Atención ' +
                  '<span>' + objDetalle.Horarios + '</span>' +
                  '</p>';

    };

    //Agregar Billetaje, solamente si tiene
    if (objDetalle.Billetaje != null && objDetalle.Billetaje.length > 0) {
      strHTML += '<p>Billetaje ' +
                   '<span>' + objDetalle.Billetaje + '</span>' +
                 '</p>';

    };

    $(PanelDetalle).html(strHTML);

    $(this.Configuracion.IDPanelDetalleEntidad).fadeIn('slow');

  },

  //Ubica el Mapa en las coordenadas correspondientes
  IrA: function (dblLatitud, dblLongitud) {
    var GoogleLatlng = new google.maps.LatLng(dblLatitud, dblLongitud);
    this.Parametros.Mapa.setCenter(GoogleLatlng)
    //Colocar un Zoom cercano zoom: 18
    this.Parametros.Mapa.setZoom(18);
  },

  //Genera, despliega y pagina lla lista de elementos (agencias, autobancos, atms, etc.)) segun el tipo seleccionado
  Configurar_Paginacion: function (nroPagina, arrDetalles) {
    var intIndice_Inicio = 0;
    var intIndice_Fin = 0;
    var intCeil = 0; //redondeo hacia arriba
    var intLongitud = arrDetalles.length; // longitud del arreglo detalles
    var arrPaginado = [];
    var boolVerSiguiente = false;
    var boolVerAtras = false;

    intCeil = Math.ceil(parseFloat(intLongitud) / parseFloat(this.Configuracion.Entidades_Paginacion.CantidadXPagina));

    //si el Nro de Pagina especificada es mayor a la posible en base a la cantidad de items y la permitida por página, entonces la página debería ser la inicial la página 1
    if (nroPagina > intCeil) nroPagina = 1; //página inicial

    //se establece el Índice Inicial
    intIndice_Inicio = (nroPagina - 1) * this.Configuracion.Entidades_Paginacion.CantidadXPagina;

    //Se establece el Índice Final
    intIndice_Fin = intIndice_Inicio + (this.Configuracion.Entidades_Paginacion.CantidadXPagina - 1);
    if (intIndice_Fin > intLongitud) intIndice_Fin = intLongitud - 1

    for (i = intIndice_Inicio; i <= intIndice_Fin; i++) {
      arrPaginado.push(arrDetalles[i]);
    };

    //Guardar el nro de Página en el span
    $(this.Configuracion.Entidades_Paginacion.IDNroPagina).val(nroPagina);

    //Mostrar los Botones si corresponde
    if (nroPagina > 1) boolVerAtras = true; else boolVerAtras = false;
    if (intCeil > nroPagina) boolVerSiguiente = true; else boolVerSiguiente = false;

    if (boolVerAtras) $(this.Configuracion.Entidades_Paginacion.IDAtras).show(); else $(this.Configuracion.Entidades_Paginacion.IDAtras).hide();
    if (boolVerSiguiente) $(this.Configuracion.Entidades_Paginacion.IDSiguiente).show(); else $(this.Configuracion.Entidades_Paginacion.IDSiguiente).hide();

    if (boolVerAtras && boolVerSiguiente) $(this.Configuracion.Entidades_Paginacion.IDSeparadorPagina).show(); else $(this.Configuracion.Entidades_Paginacion.IDSeparadorPagina).hide();

    //Crear la lista de Entidades (segun el tipo elegido)
    this.Configurar_ListaEntidades(arrPaginado);
  },

  //Genera y despliega la lista de entidades (cajeros, agencias, etc.) en dos columnas
  Configurar_ListaEntidades: function (arrDetalles) {
    var objDetalle = null;
    var objDetalleSiguiente = null;
    var intCantidadFilasDobles = 0;
    var intCantidadFilasSimples = 0;
    var intIndexUltimoItem = 0;
    var strHTML = '';
    var Entidades_TB = $(this.Configuracion.IDPanelEntidadesItems).first();

    $(Entidades_TB).html('');

    //Se tienen que ir mostrando en 2 columnas, por eso se toma el número 2 para dividir y multiplicar
    if (arrDetalles.length > 0) {
      intIndexUltimoItem = arrDetalles.length - 1;
      intCantidadFilasDobles = Math.ceil(arrDetalles.length / 2);
      intCantidadFilasSimples = (intCantidadFilasDobles * 2) - arrDetalles.length;

      //Primero Crear las Dobles
      for (i = 0; i < arrDetalles.length - intCantidadFilasSimples; i = i + 2) {
        objDetalle = arrDetalles[i];
        objDetalleSiguiente = arrDetalles[i + 1];

        $(Entidades_TB).append('<div class="Contenido">' +
                                '<div class="Celda50">' +
                                 '<a href="#_' + objDetalle.Tipo.toString() + '_' + objDetalle.Codigo.toString() + '" class="SucursalItem"  >' +
                                  '<div class="Titulo textoverde3c d15 fuente-bold">' + objDetalle.Descripcion + '</div>' +
                                  '<div class="Detalle textoplomo3d d15 ">' + objDetalle.Direccion + '</div>' +
                                 '</a>' +
                                 '</div>' +
                                 '<div class="Separador"></div>' +
                                 '<div class="Celda50">' +
                                 '<a href="#_' + objDetalleSiguiente.Tipo.toString() + '_' + objDetalleSiguiente.Codigo.toString() + '" class="SucursalItem" >' +
                                  '<div class="Titulo textoverde3c d15 fuente-bold ">' + objDetalleSiguiente.Descripcion + '</div>' +
                                  '<div class="Detalle textoplomo3d d15">' + objDetalleSiguiente.Direccion + '</div>' +
                                 '</a>' +
                                 '</div>' +
                               '</div>');
      };

      //Ahora crear las Simples
      if (intCantidadFilasSimples > 0) {
        objDetalle = arrDetalles[intIndexUltimoItem];
        $(Entidades_TB).append('<div class="Contenido">' +
                               '<div class="Celda">' +
                                '<a href="#_' + objDetalle.Tipo.toString() + '_' + objDetalle.Codigo.toString() + '"  class="SucursalItem" >' +
                                 '<div class="Titulo textoverde3c d15 fuente-bold ">' + objDetalle.Descripcion + '</div>' +
                                 '<div class="Detalle textoplomo3d d15 ">' + objDetalle.Direccion + '</div>' +
                                '</a>' +
                                '</div>' +
                              '</div>');
      };
    };

    //Mostra la Lista de Entidades
    $(this.Configuracion.IDPanelEntidades).show();

    //Solicitar a la página que se redimensione
    // COMENTADO
    //Redimensionar('ContenidoPaginaPrincipal', 'iframeCuerpo', 70);

    //Funcion que se ejecuta al hacer click sobre un elemento (cajero, agencia, etc.) de la lista desplegada
    $(Entidades_TB).find('a').click(function (e) {
      e.preventDefault();
      var arrHref = $(this).attr('href').toString().split("_");
      if (arrHref.length != 3) return false;

      //Obtener el Detalle de la Ciudad Seleccionada del tipo  y código especificado en el Link
      var objCiudad = _objGeolocalizacionBNB.Obtener_CiudadSeleccionada();
      var objDetalle = null;
      for (i = 0; i < objCiudad.Detalles.length; i++) {
        objDetalle = objCiudad.Detalles[i];

        if (objDetalle.Tipo == arrHref[1] && objDetalle.Codigo == arrHref[2]) {
          //Cerrar todos los Info del Mapa
          _objGeolocalizacionBNB.Parametros.Mapa.CerrrarTodosInfoWin();

          //Simular el Click en la Marca (abrir el InfoResumen)
          _objGeolocalizacionBNB.Simular_Click_Marcador(objDetalle);

          //Poner el mapa y marcador en las coordenadas correspondientes
          _objGeolocalizacionBNB.IrA(objDetalle.Latitud, objDetalle.Longitud);
          return true;
        };
      };
    });
  },

  //Simula la accion de hacer click sobre un marcador en el mapa para desplegar el cuadrito con la informacion resumida (InfoResumen)
  Simular_Click_Marcador: function (objDetalle) {
    //Buscar la Marca que esta en la posición y simular su Click para que muestre su info
    var objMarcador = null;
    for (i = 0; i < this.Parametros.Mapa.Marcadores.length; i++) {
      objMarcador = this.Parametros.Mapa.Marcadores[i];
      if (objMarcador.Detalle != null && objMarcador.Detalle.Tipo == objDetalle.Tipo && objMarcador.Detalle.Codigo == objDetalle.Codigo) {
        //simplemente abrir el InfoResumen de la marca, ya q es un objeto google y no tiene el método click como los objetos jquery.
        if (objMarcador.InfoResumen != null) {
          this.Mostrar_InfoResumen(objMarcador)
        };
      };
    };
  },

  //Ocultar el Area de Detalle del elemento (debajo del mapa)
  Ocultar_Detalle: function () {
    $(this.Configuracion.IDPanelDetalleEntidad).hide();
  },

  //Quitar el estilo de ciudad seleccionada en el menu de ciudades (menu de la izquierda)
  Quitar_Seleccionada: function (strQuery) {
    if (strQuery != null && strQuery.length > 0) {
      $(strQuery).each(function () {
        $(this).removeClass(_objGeolocalizacionBNB.Configuracion.Class_Seleccionada);
      });
    };
  },

  //Despliega la Descripcion (Titulo) del elemento seleccionado en el combo de Grupos (agencias, cajeros, etc.)
  Colocar_TituloFiltroSeleccionado: function () {
    var strQuery = this.Configuracion.IDcmbFiltros + ' :selected';
    var strTexto_itemSeleccionado = $(strQuery).text();
    $(this.Configuracion.IDSpanlItemSeleccionado).text(strTexto_itemSeleccionado);
  },

  //Funcionalidad del paginador (va Adelante o Atras segun lo seleccionado en pantalla)
  Paginar: function (enuTipoMovimiento) {
    var NroPaginaActual = parseInt($(this.Configuracion.Entidades_Paginacion.IDNroPagina).val());
    var objCiudad = this.Obtener_CiudadSeleccionada();
    var arrDetalles = this.Obtener_DetallesParaVer(objCiudad);

    switch (enuTipoMovimiento) {
      case TipoMover.Adelante:
        NroPaginaActual += 1;
        break;
      case TipoMover.Atras:
        NroPaginaActual -= 1;
        break;
    };

    if (NroPaginaActual <= 0) NroPaginaActual = 1;

    this.Configurar_Paginacion(NroPaginaActual, arrDetalles);

    //Quitar el Estilo Seleccionado del Primer Link en las entidades, ya que la paginación puede mostrar en otro.
    //$(this.Configuracion.IDPanelEntidadesItems + ' ul').first().find('a').first().removeClass(_objGeolocalizacionBNB.Configuracion.Class_Seleccionada);
  },

  //Selecciona en el menu de ciudades a la ciudad actualmente elegida (o ciudad recibida en el querystring)
  Seleccionar_Menu_Ciudad: function () {
    //Quitar el Estilo Seleccionado al menu de Ciudades (menu de la izquierda)
    _objGeolocalizacionBNB.Quitar_Seleccionada(_objGeolocalizacionBNB.Configuracion.Filtro_Ciudades);
    //Marcar como seleccionada a la ciudad actual
    var strQuery = _objGeolocalizacionBNB.Configuracion.Filtro_Ciudades;
    if (strQuery != null && strQuery.length > 0) {
      $(strQuery).each(function () {
        var href = $(this).attr('href').toString();
        var idciudad = href.split("_")[1];
        //Agregar el estilo de ciudad Seleccionada al Menu de Ciudades (links de la izquierda)
        if (idciudad == _objGeolocalizacionBNB.Parametros.IDCiudadSeleccionada) {
          $(this).addClass(_objGeolocalizacionBNB.Configuracion.Class_Seleccionada);
          return true;  //Para salir del loop
        };
      });
    };
  },

  //Selecciona en el Combo de Grupos (cajeros, sucursales, agencias, etc.) aquel Tab que está activo (o que fue recibido en el querystring)
  Seleccionar_Combo_Grupos: function () {
    var objTab = this.Parametros.TabSeleccionado;
    var combo = $(_objGeolocalizacionBNB.Configuracion.IDcmbFiltros).get(0);

    for (var i = 0; i < combo.length; i++) {
      var valorCombo = combo.options[i].value.split("_");
      if (valorCombo[0] == objTab.Canal && valorCombo[1] == objTab.Ver) {
        $(_objGeolocalizacionBNB.Configuracion.IDcmbFiltros).get(0).selectedIndex = combo.options[i].index;
        _objGeolocalizacionBNB.Colocar_TituloFiltroSeleccionado();
        return true;
      }
    }
  },

  //Selecciona el elemento de la lista de elementos desplegados (lista de cajeros, lista de agencias, etc.) segun el dato recibido en el querystring
  Seleccionar_Elemento: function () {
    var arrHref1 = this.ParametrosRecibidos.Tipo;
    var arrHref2 = this.ParametrosRecibidos.Elemento;
    //Obtener el Detalle de la Ciudad Seleccionada del tipo  y código especificado en el Link
    var objCiudad = _objGeolocalizacionBNB.Obtener_CiudadSeleccionada();
    var objDetalle = null;
    for (i = 0; i < objCiudad.Detalles.length; i++) {
      objDetalle = objCiudad.Detalles[i];

      if (objDetalle.Tipo == arrHref1 && objDetalle.Codigo == arrHref2) {
        //Cerrar todos los Info del Mapa
        _objGeolocalizacionBNB.Parametros.Mapa.CerrrarTodosInfoWin();

        //Simular el Click en la Marca (abrir el InfoResumen)
        _objGeolocalizacionBNB.Simular_Click_Marcador(objDetalle);

        //Poner el mapa y marcador en las coordenadas correspondientes
        _objGeolocalizacionBNB.IrA(objDetalle.Latitud, objDetalle.Longitud);
        return true;
      };
    };
  },

};

//Al terminar de cargar el Documento, se llama a la carga del Objeto
$(document).ready(function () { _objGeolocalizacionBNB.Cargar(); });


