﻿using BNB_PortalExterno.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BNB_PortalExterno
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            //Ruta Servidor de Simulacion
            this.Application["SimulationServer"] = ConfigurationManager.AppSettings["SimulationServer"];
            this.Application["CRMServer"] = ConfigurationManager.AppSettings["CRMServer"];
            //Obtener la estrucutura del menu y almacenarlo en una variable de aplicacion.
            Componentes.Metodos.Menu.Obtener.TipoRespuesta TipoRespuestaObtenerMenu = Componentes.LogicaNegocios.Menu.Obtener();
            if (Application[Models.Constantes.MENU] == null && TipoRespuestaObtenerMenu.OcurrioError == false)
            {
                Application[Models.Constantes.MENU] = TipoRespuestaObtenerMenu.Menu;
            }
            //Obtener la lista de los detalles de cada item.
            Componentes.Metodos.DetalleItemMenu.Obtener.TipoRespuesta TipoRespuestaObtenerDetalleItemMenu = Componentes.LogicaNegocios.DetalleItemMenu.Obtener();
            if (Application[Models.Constantes.DETALLES_ITEM_MENU] == null && TipoRespuestaObtenerDetalleItemMenu.OcurrioError == false)
            {
                Application[Models.Constantes.DETALLES_ITEM_MENU] = TipoRespuestaObtenerDetalleItemMenu.DetalleItemsMenu;
            }
            //Obtener la informacion de las parametricas
            Componentes.Metodos.Parametros.Obtener.TipoRespuesta TipoRespuesObtenerParametros = Componentes.LogicaNegocios.Parametros.Obtener();
            if (Application[Models.Constantes.PARAMETROS] == null && TipoRespuesObtenerParametros.OcurrioError == false)
            {
                Application[Models.Constantes.PARAMETROS] = TipoRespuesObtenerParametros.Parametros;
            }
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
        }


        protected void Session_Start(object sender, EventArgs e)
        {
            Session[Models.Constantes.MENU] = Application[Models.Constantes.MENU] as List<Models.Menu>;
            Session[Models.Constantes.DETALLES_ITEM_MENU] = Session[Models.Constantes.DETALLES_ITEM_MENU] as List<Models.DetalleItemMenu>;
            //Session[Componentes.Definiciones.Constantes.SUSCRIPCION] = new AgenciaDigitalZeta.Models.Suscripcion();
            ////Inicializar la variable de sesion para controlar la informacion de acceso
            //Models.Acceso Acceso = new Models.Acceso();
            //Acceso.Ip = ObtenerIp();
            //Acceso.FechaAcceso = DateTime.UtcNow;
            //Acceso.Descripcion = DateTime.Now.ToString();
            ////Almacenar la informacion de acceso
            //Componentes.LogicaNegocios.Acceso.Registrar(Acceso.Ip, Acceso.FechaAcceso, Acceso.Descripcion);
        }


        protected void Application_Error(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("Enter - Application_Error");

            var httpContext = ((MvcApplication)sender).Context;
            httpContext.Response.ContentType = "text/html";

            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));
            var currentController = " ";
            var currentAction = " ";

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null &&
                    !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null &&
                    !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }

            var ex = Server.GetLastError();

            if (ex != null)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);

                if (ex.InnerException != null)
                {
                    System.Diagnostics.Trace.WriteLine(ex.InnerException);
                    System.Diagnostics.Trace.WriteLine(ex.InnerException.Message);
                }
            }

            var controller = new ErrorController();
            var routeData = new RouteData();
            var action = "CustomError";
            var statusCode = 500;

            if (ex is HttpException)
            {
                var httpEx = ex as HttpException;
                statusCode = httpEx.GetHttpCode();
                switch (httpEx.GetHttpCode())
                {
                    case 400:
                        action = "CustomError";
                        break;
                    case 401:
                        action = "Unauthorized";
                        break;
                    case 403:
                        action = "Forbidden";
                        break;
                    case 404:
                        action = "PageNotFound";
                        break;

                    case 500:
                        action = "CustomError";
                        break;
                    default:
                        action = "CustomError";
                        break;
                }
            }
            else if (ex is AuthenticationException)
            {
                action = "Forbidden";
                statusCode = 403;
            }

            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = statusCode;
            httpContext.Response.TrySkipIisCustomErrors = true;
            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = action;

            controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }


    }
}
